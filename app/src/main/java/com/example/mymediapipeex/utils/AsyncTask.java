package com.example.mymediapipeex.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AsyncTask {
    //  리액티브 비동기 스레드 객체
    public static Disposable mBackgroundtask;

    /**
     * 동기화 처리 함수
     * @param asyncTaskITF - 수행 내용 인터페이스
     */
    public static void task(AsyncTaskInterface asyncTaskITF) {
        mBackgroundtask = Observable.fromCallable(() -> {
                    /** doInBackground    **/
                    asyncTaskITF.doInBackground();
                    return false;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(throwable -> false)
                .subscribe((result) -> {
                    /** onPostExecute    **/
                    asyncTaskITF.onPostExecute();
                });
    }
}
