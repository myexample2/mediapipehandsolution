package com.example.mymediapipeex.utils;

import android.util.Log;

import com.example.mymediapipeex.BuildConfig;

/**
 * Created by TCH on 2020-07-01
 *
 * @author think.code.help @gmail.com
 * @version 1.0
 * @since 2020 -07-01
 */
public class MDEBUG {
    /**
     * Debug Mode
     */
    private static boolean isDebug =
            BuildConfig.IS_DEBUG;

    /**
     * debug
     *
     * @param _str the str
     * @Comment : Debug Message, 호출한 파일 출력
     */

    public static void d(String _str) {
        if (isDebug) {
            Exception e = new Exception();
            StackTraceElement element = e.getStackTrace()[1];
            Log.d("debug", element.getFileName() + "=>  " +  buildLogMsg(_str));
        }
    }

    /**
     * error
     *
     * @param _str the str
     * @Comment : Error Message 출력
     */
    public static void error(String _str) {
        if (isDebug) {
            Log.e("error", "=========>" + _str);
        }
    }

    /**
     * 해당 함수 호출 한 라인 추가
     * @param message - Print Msg
     * @return - 출력 할 함수 + Msg
     */
    public static String buildLogMsg(String message) {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        StringBuilder sb = new StringBuilder();
        sb.append(message);

        sb.append("       [");

        sb.append(ste.getFileName());

        sb.append(" > ");

        sb.append(ste.getMethodName());

        sb.append(" > #");

        sb.append(ste.getLineNumber());

        sb.append("]");
        return sb.toString();
    }
}