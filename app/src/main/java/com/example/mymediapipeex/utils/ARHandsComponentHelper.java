package com.example.mymediapipeex.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;

import com.example.mymediapipeex.WattHandTracking;

/**
 * AR Hands 사용 편의성 위한 Helper 객체
 */
public class ARHandsComponentHelper {

    public ARHandsComponentHelper() {
    }

    public static void startHand(Activity activity, View view, WattHandTracking.HandGestureListener gestureListener, AiUIListener aiClickListener) {
        WattHandTracking.getInstance().initHands(activity, view, gestureListener);
        if (aiClickListener != null) {
            aiClickListener.setAIListener();
            aiClickListener.setScrollListener();
        }
    }

    /**
     * AR 기능 등록 된 Dialog Show
     * 다이얼로그 실행 될 때 해당 액티비티 AR 기능 끈 후 다이얼로그 종료 시 다시 시작
     * (View 이동 시 해당 View 값으로 다시 AR 기능을 Reload 하여야만 한다.)
     * @param activity - act
     * @param dialog - dialog
     * @param gestureListener - 제스쳐 리스너 (호출 객체 구현 부)
     * @param aiClickListener - AI 클릭 리스너 (호출 객체 부현 부)
     */
    public static void showARDialog(Activity activity, Dialog dialog, WattHandTracking.HandGestureListener gestureListener, AiUIListener aiClickListener) {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
            dialog.setOnDismissListener(arg -> {
                startHand(activity, activity.getWindow().getDecorView(), gestureListener, aiClickListener);
            });
        }
    }

    public interface AiUIListener {
        public void setAIListener();

        public void setScrollListener();
    }
}
