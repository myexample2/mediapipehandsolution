package com.example.mymediapipeex.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.mymediapipeex.WattHandTracking;

public class PrefManager {

    public static PrefManager mPrefManager;

    private Context context;

    public static final String PREF_NAME = "ar_hands";

    /** 일상 점검 체크리스트 항목 저장 값 **/
    public static final String PREF_KEY_DATE = "";
    public static final String PREF_KEY_METHOD = "";    // 점검 방법
    public static final String PREF_KEY_IMPACT = "";    // 점검 영향
    public static final String PREF_KEY_CRITERIA = "";  // 판단 기준
    public static final String PREF_KEY_PERIOD = "";    // 점검 주기

    public PrefManager(Context context) {
        this.context = context;
    }

    // Singleton Instance Getter
    public static PrefManager getInstance(Context context) {
        if (mPrefManager == null) {
            mPrefManager = new PrefManager(context);
        }
        return mPrefManager;
    }

    private SharedPreferences getPreference(){
        if (context != null){
            return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
        return null;
    }

    public boolean isContains(String valueName) {
        if (valueName != null) {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                return preference.contains(valueName);
            } else {
                return false;
            }

        }
        return false;
    }

    public boolean clearAll() {
        SharedPreferences preferences = getPreference();
        if (preferences != null) {
            preferences.edit().clear().commit();
        }
        return false;
    }

    public boolean getBoolean(String valueName, boolean defValue) {
        if (valueName != null) {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                return preference.getBoolean(valueName, defValue);
            }
        }
        return defValue;
    }

    public boolean setBoolean(String valueName, boolean value) {
        if (valueName != null) {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                SharedPreferences.Editor editor = preference.edit();
                if (editor != null) {
                    editor.putBoolean(valueName, value);
                    editor.apply();
                    return true;
                }
            }
        }
        return false;
    }

    public boolean setString(String valueName, String value) {
        if (valueName != null) {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                SharedPreferences.Editor editor = preference.edit();
                if (editor != null) {
                    editor.putString(valueName, value);
                    editor.apply();
                    return true;
                }
            }
        }
        return false;
    }

    public String getString(String valueName, String defValue) {
        if (valueName != null) {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                return preference.getString(valueName, defValue);
            }
        }
        return defValue;
    }

    public boolean setInt(String key, int value) {
        SharedPreferences preference = getPreference();
        if (preference != null) {
            SharedPreferences.Editor editor = preference.edit();
            if (editor != null) {
                editor.putInt(key, value);
                editor.apply();
                return true;
            }
        }
        return false;
    }

    public int getInt(String key, int defValue) {
        int returnVal = defValue;
        try {
            SharedPreferences preference = getPreference();
            if (preference != null) {
                returnVal = preference.getInt(key, defValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnVal;
    }
}
