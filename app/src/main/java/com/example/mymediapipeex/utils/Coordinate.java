package com.example.mymediapipeex.utils;

import android.graphics.PointF;

public class Coordinate {

    /**
     * 두 지점 사이의 거리 반환
     */
    public static double getDistance(int x, int y, int x1, int y1) {
        return Math.sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1));
    }

    /**
     * 세 점 사이의 각도 계산
     * @param p1 포인트 1
     * @param centerP 중심 점
     * @param p2 포인트 2
     * @return 각도
     */
    public static float getAngleFromThreePoints(PointF p1, PointF centerP, PointF p2){
        float p12 = (float) Math.sqrt(Math.pow(p1.x - centerP.x, 2) + Math.pow(p1.y - centerP.y, 2));
        float p23 = (float) Math.sqrt(Math.pow(centerP.x - p2.x, 2) + Math.pow(centerP.y - p2.y, 2));
        float p31 = (float) Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
        float radian = (float) Math.acos((p12*p12 + p23*p23 - p31*p31) / (2 * p12 * p23));
        float degree = (float) (radian / Math.PI * 180);
        return degree;
    }

    /**
     * 좌표 민감도 값 보정
     *
     * @param val   - 변환 하려는 값
     * @param ratio - 변환 값 배율
     * @return - 보정 값
     */
    public static float posReflection(float val, float ratio) {
        if (val < 0 || val > 1) return val;
        float calibrateVal = val; // 보정 값

        // NOTE - 0.6 == 상단으로 치우친 뷰 (0.5 값 기준으로 위아래 포인팅팅)
       calibrateVal += ((val - 0.6) / ratio); // opengl pos 값 민감도 설정

        return calibrateVal;
    }

    /**
     * 감지된 #d Z좌표 보정을 위한 함수
     * 거리에 따라 좌표 이동 반경 수정
     * 거리가 가까우면 두 손가락 포인트 겹치기가 어려워 z값을 통해 보정
     * 가까운 경우 -> 이동 감도 낮게, 거리가 먼 경우 -> 감도 빠르게
     *
     * @param val - 보정 하려는 값
     * @param z   - 감지된 z 값
     * @return 3D 좌표 보정 된 값
     */
    public static float zValueReflection(float val, float z) {
        if (z > 0) return val; // 먼 경우는 보정 하지 않음

        z = Math.abs(z); // 절대 값으로 계산
        z /= 10;  // z값에 의존하는 값 수치

        float calibrate = val;
        if (val > 0.5) { //  = 음수인 경우
            calibrate += z; // 0.5에 가까워 져야 하므로 + 되도록
            if (val > 0.5) return val;  // 해당 보정에 의해 좌 우 가 넘어간 경우 원본 값 return
        } else { // 0.5에 가까워져야 하므로 - 연산
            calibrate -= z;
            if (val < 0.5) return val;  // 해당 보정에 의해 좌 우 가 넘어간 경우 원본 값 return
        }

        return calibrate; // 해당 보정에 의해 좌 우 가 변하지 않은 경우 보정 된 값 return
    }
}
