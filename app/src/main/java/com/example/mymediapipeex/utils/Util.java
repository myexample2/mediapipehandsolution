package com.example.mymediapipeex.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String getTime() {
        Date dt = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss");
        return timeFormat.format(dt).toString();
    }
}
