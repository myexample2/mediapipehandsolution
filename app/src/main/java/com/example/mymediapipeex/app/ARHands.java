package com.example.mymediapipeex.app;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.mymediapipeex.db.MAppDatabase;

/**
 * The type
 *
 */
public class ARHands extends Application {

    private static Context context;

    /**
     * Room DataBase
     */
    public static MAppDatabase mAppDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        ARHands.context = getApplicationContext();

        mAppDatabase = Room.databaseBuilder(this,
                MAppDatabase.class, "CHECK_LIST").fallbackToDestructiveMigration().
                allowMainThreadQueries().build();
    }

    public static Context getContext(){
        return ARHands.context;
    }
}
