package com.example.mymediapipeex.data;

/**
 * Check List Gruop
 */
public class InspectionCheckList {
    public String mTitleName = ""; //  타이틀 이름
    public String mInputTitle = ""; //  타이틀 이름
    public String mMethodInputTitle = "입력"; // 정검 방법 Input Title
    public String mCriteriaInputTitle = "입력"; // 판단 기준 Input Title

    public String mMethod = ""; // 점검 방법
    public String mImpact = ""; // 점검 영향
    public String mCriteria = ""; // 판단 기준
    public String mPeriod = ""; // 점검 주기

    public InspectionCheckList(String name){
        mTitleName = name;
    }
}
