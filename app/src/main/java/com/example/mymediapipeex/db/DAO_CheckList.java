package com.example.mymediapipeex.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DAO_CheckList {
    @Insert
    void insert(DTO_CheckList dto_checkList);

    @Delete
    void delete(DTO_CheckList... dto_checkList);

    @Query("SELECT * FROM DTO_CheckList ORDER BY idx_ ASC LIMIT 999")
    List<DTO_CheckList> getAll();

    @Query("SELECT DISTINCT time FROM DTO_CheckList ORDER BY idx_ ASC LIMIT 999")
    List<String> getList();

    @Query("SELECT * FROM DTO_CheckList WHERE time = :time ORDER BY idx_ ASC LIMIT 999")
    List<DTO_CheckList> getDetail(String time);

    @Query("DELETE FROM DTO_CheckList WHERE time = :time")
    void delete(String time);

    @Query("DELETE FROM DTO_CheckList")
    void deleteAll();
}