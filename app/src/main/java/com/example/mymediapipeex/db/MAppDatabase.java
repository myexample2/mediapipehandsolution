package com.example.mymediapipeex.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 */
@Database(entities = {DTO_CheckList.class}, version = 4, exportSchema = false)
public abstract class MAppDatabase extends RoomDatabase {
	/**
	 * M dao handler dao handler.
	 *
	 * @return the dao handler
	 */
	public abstract DAO_CheckList dao_checkList();

	private static volatile MAppDatabase INSTANCE;
}