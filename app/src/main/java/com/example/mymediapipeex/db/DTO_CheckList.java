package com.example.mymediapipeex.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DTO_CheckList {
    /**
     * The Idx.
     */
    @PrimaryKey(autoGenerate = true)
    public int idx_;

    /**
     * 내용
     */
    @ColumnInfo(name = "title")
    public String mTitle;

    /**
     * 점검 방법
     */
    @ColumnInfo(name = "method")
    public String mMethod;

    /**
     * 점검 영향
     */
    @ColumnInfo(name = "impact")
    public String mImpact;

    /**
     * 판단 기준
     */
    @ColumnInfo(name = "criteria")
    public String mCriteria;

    /**
     * 점검 주기
     */
    @ColumnInfo(name = "period")
    public String mPeriod;

    /**
     * 저장 시간
     */
    @ColumnInfo(name = "time")
    public String mCreateAt;

    /**
     * 작성자
     */
    @ColumnInfo(name = "userName")
    public String mUserName;

    public DTO_CheckList(String mCreateAt, String mUserName, String mDeviceName) {
        this.mCreateAt = mCreateAt;
        this.mUserName = mUserName;
        this.mDeviceName = mDeviceName;
    }

    /**
     * 장비명
     */
    @ColumnInfo(name = "deviceName")
    public String mDeviceName;



    public DTO_CheckList(String mTitle, String mMethod, String mImpact, String mCriteria, String mPeriod) {
        this.mTitle = mTitle;
        this.mMethod = mMethod;
        this.mImpact = mImpact;
        this.mCriteria = mCriteria;
        this.mPeriod = mPeriod;
    }

    public DTO_CheckList() {

    }

    public DTO_CheckList(String time) {
        this.mCreateAt = time;
    }
}
