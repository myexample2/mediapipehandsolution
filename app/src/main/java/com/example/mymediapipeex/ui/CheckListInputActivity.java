package com.example.mymediapipeex.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.mymediapipeex.EditText_;
import com.example.mymediapipeex.R;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.adapter.CheckListInputAdapter;
import com.example.mymediapipeex.app.ARHands;
import com.example.mymediapipeex.data.InspectionCheckList;
import com.example.mymediapipeex.db.DTO_CheckList;
import com.example.mymediapipeex.utils.ARHandsComponentHelper;
import com.example.mymediapipeex.utils.MDEBUG;
import com.example.mymediapipeex.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckListInputActivity extends AppCompatActivity {
    private RecyclerView mListView;
    private CheckListInputAdapter mCheckListAdapter;
    private ArrayList<InspectionCheckList> mList = new ArrayList<InspectionCheckList>();

    // 핸드 트래킹 처리 객체
    WattHandTracking mWattHandTracking;

    // 키패드 UI
    @BindView(R.id.et_memo_input)
    EditText_ et_memo_input;

    // 저장 버튼
    @BindView(R.id.btnListSave)
    Button btnListSave;

    // 뒤로가기 버튼
    @BindView(R.id.layout_back)
    LinearLayout layout_back;

    // 키패드 호출 한 view 값
    int mKeypadViewStatus = -1;
    int mKeypadIndexStatus = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MDEBUG.d("## onCreate");
        // 전체화면으로 보여주기 위해 셋팅한다.
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_checklist_input);
        ButterKnife.bind(this);
        mWattHandTracking = WattHandTracking.getInstance();
        initUI();
        setDummyData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MDEBUG.d("# Input onResume");
        if (mWattHandTracking.checkViewChanges(findViewById(R.id.hand_draw_view))) {
            ARHandsComponentHelper.startHand(this, getWindow().getDecorView(), mHandGestureListener, aiClickListener);
            mCheckListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MDEBUG.d("# Input onPause");
    }

    private void initUI() {
        mCheckListAdapter = new CheckListInputAdapter(this, mList, mWattHandTracking);
        mListView = findViewById(R.id.recycleCheckList);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setLayoutManager(llm);
        mListView.setAdapter(mCheckListAdapter);
        // 키패드 입력 받은 내용 삽입
        et_memo_input.setOnCloseListener(() -> {
            switch (mKeypadViewStatus) {
                case R.id.rb_criteria_input:
                    MDEBUG.d("setOnCloseListener!!");
                    mList.get(mKeypadIndexStatus).mCriteriaInputTitle = et_memo_input.getText().toString();
                    mList.get(mKeypadIndexStatus).mCriteria = et_memo_input.getText().toString();
                    break;
                case R.id.rb_method_input:
                    mList.get(mKeypadIndexStatus).mMethodInputTitle = et_memo_input.getText().toString();
                    mList.get(mKeypadIndexStatus).mMethod = et_memo_input.getText().toString();
                    break;
            }
            onResume(); // Dialog 종료 시 AR Hand 다시 키기 위한 솔루션
            et_memo_input.setText("");
            mCheckListAdapter.notifyDataSetChanged();
            return null;
        });
        et_memo_input.setMActivity(this);
        mCheckListAdapter.setInPutClick((view, index) -> {
            mWattHandTracking.stopHands();
            et_memo_input.start();
            mKeypadViewStatus = view.getId();
            mKeypadIndexStatus = index;
        });
    }

    /**
     * AI UI Listener
     */
    ARHandsComponentHelper.AiUIListener aiClickListener = new ARHandsComponentHelper.AiUIListener() {
        @Override
        public void setAIListener() {
            mWattHandTracking.setAiClickListener(btnListSave);
            mWattHandTracking.setAiClickListener(layout_back);
        }

        @Override
        public void setScrollListener() {
            mWattHandTracking.setAiScrollUIListener(mListView);
        }
    };


    @OnClick({R.id.btnListSave, R.id.layout_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnListSave:
//                ARHands.mAppDatabase.dao_checkList().deleteAll();
                for (InspectionCheckList e : mList) {
                    DTO_CheckList col = new DTO_CheckList();
                    col.mTitle = e.mTitleName;
                    col.mMethod = e.mMethod;
                    col.mImpact = e.mImpact;
                    col.mCriteria = e.mCriteria;
                    col.mPeriod = e.mPeriod;
                    col.mCreateAt = Util.getTime();
                    ARHands.mAppDatabase.dao_checkList().insert(col);
                }
                CustomToast.makeText(getApplicationContext(),"점검 항목이 저장 되었습니다",
                        CustomToast.LONG,CustomToast.SUCCESS,true).show();
                onBackPressed();
                break;
            case R.id.layout_back:
                onBackPressed();
                break;
        }
    }

    private void setDummyData() {
        mList.add(new InspectionCheckList("설비메인에어압력 이상은 없는가?"));
        mList.add(new InspectionCheckList("열풍air압력계는 정상작동하나?"));
        mList.add(new InspectionCheckList("비상 및 안전센서 작동 이상은 없는가?"));
        mList.add(new InspectionCheckList("작동부 윤활상태는 이상 없는가?"));
        mList.add(new InspectionCheckList("보스 융착부 융착상태 이상없는가?"));
        mList.add(new InspectionCheckList("Air cylinder 및 Sol v/v 작동상태는?"));
        mList.add(new InspectionCheckList("융착팁 풀림상태 확인"));
        mList.add(new InspectionCheckList("냉각AIR노즐 위치확인"));
        mList.add(new InspectionCheckList("볼트풀림,I-마킹 이상없는가?"));
        mList.add(new InspectionCheckList("가이드포스트 갭 발생여부 점검"));

        mCheckListAdapter.notifyDataSetChanged();
        MDEBUG.d("## size : " + mCheckListAdapter.mList.size());
    }

    WattHandTracking.HandGestureListener mHandGestureListener = new WattHandTracking.HandGestureListener() {
        @Override
        public void move(int moveValue, float scrollX, float scrollY, View view) {
            if (view == mListView)
                runOnUiThread(() -> {
                    mListView.scrollBy(0, (int) (scrollY / 1.7));
                });
        }

        @Override
        public void zoom(double value) {

        }

        @Override
        public void getFingerPoint(Point[] fingerPoint) {

        }
    };
}