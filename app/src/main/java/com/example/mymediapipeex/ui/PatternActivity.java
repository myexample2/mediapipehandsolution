package com.example.mymediapipeex.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mymediapipeex.R;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.utils.ARHandsComponentHelper;
import com.example.mymediapipeex.utils.MDEBUG;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PatternActivity extends AppCompatActivity {
    private static final String TAG = "Pattern";

    // 핸드 트래킹 처리 객체
    WattHandTracking mWattHandTracking;

    public FrameLayout patternView;
    public TextView tv_re_input, tv_ok, tv_cancel, tvUserName, tvPatternTitle, tvMinPatternTitle;

    public TextView[] mTvPassword = new TextView[4]; // 패턴 비밀번호
    public TextView[] mPatternNumber = new TextView[9];
    public ImageView[] mPatternImg = new ImageView[9];

    int[] randomNumber = new int[9]; //int형 배열 선언

    // 임시 비밀번호
    String mTempPw = "5645";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pattern);
        init(true);
        mWattHandTracking = WattHandTracking.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWattHandTracking.checkViewChanges(findViewById(R.id.hand_draw_view))) {
            ARHandsComponentHelper.startHand(this, getWindow().getDecorView(), null, aiClickListener);
        }
    }

    /**
     * AI UI Listener
     */
    ARHandsComponentHelper.AiUIListener aiClickListener = new ARHandsComponentHelper.AiUIListener() {
        @Override
        public void setAIListener() {
            mWattHandTracking.setAiClickListener(tv_re_input);
            mWattHandTracking.setAiClickListener(tv_ok);
            mWattHandTracking.setAiClickListener(tv_cancel);
            for (TextView tvPattern : mPatternNumber)
                mWattHandTracking.setAiClickListener(tvPattern);
        }

        @Override
        public void setScrollListener() {
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (TextView tvPattern : mPatternNumber) {
                if (v.getId() == tvPattern.getId()) {
                    MDEBUG.d("tv :" + tvPattern.getTag());
                    for (TextView tvPw : mTvPassword) {
                        if (tvPw.getText().length() <= 0) {
                            tvPw.setText(tvPattern.getTag().toString());
                            break;
                        }
                    }
                }
            }

            switch (v.getId()) {
                case R.id.tv_re_input:
                    selectPatternClear();
                    break;
                case R.id.tv_cancel:
                    for (int i = mTvPassword.length - 1; i >= 0; i--) {
                        TextView tvPw = mTvPassword[i];
                        if (tvPw.getText().length() > 0) {
                            tvPw.setText("");
                            break;
                        }
                    }
                    break;
                case R.id.tv_ok:
                    loginTry();
                    break;
            }
        }
    };

    public void init(boolean isDisplaySize480p) {
        patternView = (FrameLayout) findViewById(R.id.pattern_view);
        tv_re_input = (TextView) findViewById(R.id.tv_re_input);
        tv_ok = (TextView) findViewById(R.id.tv_ok);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvPatternTitle = (TextView) findViewById(R.id.tv_pattern_title);
        tvMinPatternTitle = (TextView) findViewById(R.id.tv_min_pattern_title);

        mPatternNumber[0] = (TextView) findViewById(R.id.pattern_num_01);
        mPatternNumber[1] = (TextView) findViewById(R.id.pattern_num_02);
        mPatternNumber[2] = (TextView) findViewById(R.id.pattern_num_03);
        mPatternNumber[3] = (TextView) findViewById(R.id.pattern_num_04);
        mPatternNumber[4] = (TextView) findViewById(R.id.pattern_num_05);
        mPatternNumber[5] = (TextView) findViewById(R.id.pattern_num_06);
        mPatternNumber[6] = (TextView) findViewById(R.id.pattern_num_07);
        mPatternNumber[7] = (TextView) findViewById(R.id.pattern_num_08);
        mPatternNumber[8] = (TextView) findViewById(R.id.pattern_num_09);

        mPatternImg[0] = (ImageView) findViewById(R.id.iv_01);
        mPatternImg[1] = (ImageView) findViewById(R.id.iv_02);
        mPatternImg[2] = (ImageView) findViewById(R.id.iv_03);
        mPatternImg[3] = (ImageView) findViewById(R.id.iv_04);
        mPatternImg[4] = (ImageView) findViewById(R.id.iv_05);
        mPatternImg[5] = (ImageView) findViewById(R.id.iv_06);
        mPatternImg[6] = (ImageView) findViewById(R.id.iv_07);
        mPatternImg[7] = (ImageView) findViewById(R.id.iv_08);
        mPatternImg[8] = (ImageView) findViewById(R.id.iv_09);

        mTvPassword[0] = (TextView) findViewById(R.id.etPasscode1);
        mTvPassword[1] = (TextView) findViewById(R.id.etPasscode2);
        mTvPassword[2] = (TextView) findViewById(R.id.etPasscode3);
        mTvPassword[3] = (TextView) findViewById(R.id.etPasscode4);

//        patternErrorDialog = new CheckUserDialog(this);

        for (int i = 0; i <= 8; i++) {
            mPatternNumber[i].setOnClickListener(onClickListener);
        }
        createRandomNumber();

        tv_re_input.setOnClickListener(onClickListener);
        tv_ok.setOnClickListener(onClickListener);
        tv_cancel.setOnClickListener(onClickListener);

//        tvUserName.setText(sName + " ,");
    }

    /**
     * Clear
     */
    public void selectPatternClear() {
        for (TextView tv : mTvPassword)
            tv.setText("");
    }

    /**
     * 입력 되어있는 패턴으로 로그인 시도
     */
    public void loginTry() {
        String pw = "";
        for (TextView tv : mTvPassword)
            pw += tv.getText().toString();
        MDEBUG.d("pw : " + pw);

        if (pw.length() < 4) { // 모두 입력하지 않은 경우
            CustomToast.makeText(PatternActivity.this, "4자리 숫자를 모두 입력해주세요.",
                    CustomToast.LONG, CustomToast.SUCCESS, true).show();
        } else if (!pw.equals(mTempPw)) { // 패스워드 불일치
            CustomToast.makeText(PatternActivity.this, "패스워드가 일치하지 않습니다.",
                    CustomToast.LONG, CustomToast.SUCCESS, true).show();
        } else { // 로그인 성공
            CustomToast.makeText(PatternActivity.this, "로그인 성공.",
                    CustomToast.LONG, CustomToast.SUCCESS, true).show();
            Intent intent = new Intent();
            intent.putExtra("result", "login_ok");
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void createRandomNumber() {
        Random r = new Random(); //객체생성
        for (int i = 0; i <= 8; i++)    //숫자 6개를 뽑기위한 for문
        {
            randomNumber[i] = r.nextInt(9) + 1; //1~10숫자중 랜덤으로 하나를 뽑아 a[0]~a[5]에 저장
            for (int j = 0; j < i; j++) //중복제거를 위한 for문
            {
                if (randomNumber[i] == randomNumber[j]) {
                    i--;
                }
            }
        }
        patternNumberSet();
    }

    public void patternNumberSet() {
        for (int i = 0; i <= 8; i++) {
            mPatternNumber[i].setText(String.valueOf(randomNumber[i]));
            mPatternNumber[i].setTag(randomNumber[i]);
        }
    }

}
