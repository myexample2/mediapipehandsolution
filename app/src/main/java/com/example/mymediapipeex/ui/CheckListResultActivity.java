package com.example.mymediapipeex.ui;

import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymediapipeex.R;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.adapter.CheckListResultAdapter;
import com.example.mymediapipeex.app.ARHands;
import com.example.mymediapipeex.db.DTO_CheckList;
import com.example.mymediapipeex.utils.ARHandsComponentHelper;
import com.example.mymediapipeex.utils.MDEBUG;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckListResultActivity extends AppCompatActivity {
    // 하나의 Adapter로 시간별, 항목 별 표시 위해 Temp Array List 할당
    private List<DTO_CheckList> mList = new ArrayList<>(); // View 표시되는 리스트

    CheckListResultAdapter mCheckListResultAdapter;
    private RecyclerView mListView;

    // 핸드 트래킹 처리 객체
    WattHandTracking mWattHandTracking;

    // 뒤로가기 버튼
    @BindView(R.id.layout_back)
    LinearLayout layout_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_checklist_result);
        ButterKnife.bind(this);
        mWattHandTracking = WattHandTracking.getInstance();
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MDEBUG.d("# Result onResume");
        if (mWattHandTracking.checkViewChanges(findViewById(R.id.hand_draw_view))) {
            MDEBUG.d("#check View Chandges ");
            ARHandsComponentHelper.startHand(this, getWindow().getDecorView(), mHandGestureListener, aiClickListener);
            mCheckListResultAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MDEBUG.d("# Result onPause");
    }

    private void initUI() {
        MDEBUG.d("initUI");
        mListView = findViewById(R.id.recycleCheckListResult);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setLayoutManager(llm);
        mCheckListResultAdapter = new CheckListResultAdapter(getApplicationContext(), mList);
        getList();
        mListView.setAdapter(mCheckListResultAdapter);

        mCheckListResultAdapter.setItemClick((view, index) -> {
            if (view.getId() == R.id.ll_time_list)
                getDetail(mList.get(index).mCreateAt);
            else if (view.getId() == R.id.ll_checklist_delete)
                deleteRow(mList.get(index).mCreateAt);
        });
    }

    /**
     * AI UI Listener
     */
    ARHandsComponentHelper.AiUIListener aiClickListener = new ARHandsComponentHelper.AiUIListener() {
        @Override
        public void setAIListener() {
            mWattHandTracking.setAiClickListener(layout_back);
        }

        @Override
        public void setScrollListener() {
            mWattHandTracking.setAiScrollUIListener(mListView);
        }
    };

    @OnClick({R.id.layout_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_back:
                backClickProcess();
                break;
        }
    }

    /**
     * 체크 리스트 항목 DB Get
     */
    private void getList() {
        mWattHandTracking.initAIClickListener(); // View 변경 시 AI 클릭 이벤트 처리도 같이 변화되도록 설정
        aiClickListener.setAIListener();
        mCheckListResultAdapter.mIsViewDetail = false;
        mList.clear();
        for (String time : ARHands.mAppDatabase.dao_checkList().getList())  // 등록 된 시간별 리스트 Getter
            mList.add(new DTO_CheckList(time, "최성빈", "FRT LH 열풍융착"));
        mList.add(0, new DTO_CheckList("기록 시간", "작성자", "장비명"));

        mCheckListResultAdapter.notifyDataSetChanged();
    }

    /**
     * 체크 리스트 Detial DB Get
     */
    private void getDetail(String time) {
        mWattHandTracking.initAIClickListener(); // View 변경 시 AI 클릭 이벤트 처리도 같이 변화되도록 설정
        aiClickListener.setAIListener();
        mCheckListResultAdapter.mIsViewDetail = true;
        mList = ARHands.mAppDatabase.dao_checkList().getDetail(time);
        MDEBUG.d("mList len : " + mList.size());
        mList.add(0, new DTO_CheckList("점검항목", "점검 방법", "영향", "판단 기준", "점검 주기"));
        mCheckListResultAdapter.setmList(mList);
        mCheckListResultAdapter.notifyDataSetChanged();
    }

    /**
     * 해당 행 삭제
     * @param time - 삭제 할 시간 값 (시간 값으로 식별)
     */
    private void deleteRow(String time) {
        MDEBUG.d("time : " + time);
        ARHands.mAppDatabase.dao_checkList().delete(time);
        getList();
    }

    /**
     * 뒤로 가기 버튼 클릭 처리
     */
    private void backClickProcess() {
        // 디테일 표시 되고 있는 경우 -> 항목별 표시로 변환
        if (mCheckListResultAdapter.mIsViewDetail) {
            getList();
        } else  // 항목 별 표시 되고 있는 경우 -> 해당 액티비티 종료
            onBackPressed();

        MDEBUG.d("# mCheckListResultAdapter.mIsViewDetail : " + mCheckListResultAdapter.mIsViewDetail);
        for (DTO_CheckList e : mList) {
            MDEBUG.d("# time : " + e.mCreateAt);
        }
    }

    WattHandTracking.HandGestureListener mHandGestureListener = new WattHandTracking.HandGestureListener() {
        @Override
        public void move(int moveValue, float scrollX, float scrollY, View view) {
            runOnUiThread(() -> {
                mListView.scrollBy(0, (int) (scrollY / 1.7));
            });
        }

        @Override
        public void zoom(double value) {

        }

        @Override
        public void getFingerPoint(Point[] fingerPoint) {

        }
    };
}
