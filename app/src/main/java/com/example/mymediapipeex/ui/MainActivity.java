// Copyright 2021 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.example.mymediapipeex.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.core.content.ContextCompat;

import com.example.mymediapipeex.ImageViewerActivity;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.R;
import com.example.mymediapipeex.constant.CAMERA;
import com.example.mymediapipeex.constant.FINGER;
import com.example.mymediapipeex.permission.PermissionActivity;
import com.example.mymediapipeex.utils.ARHandsComponentHelper;
import com.example.mymediapipeex.utils.MDEBUG;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 *
 * Main activity of MediaPipe Hands app.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    // 핸드 트래킹 처리 객체
    WattHandTracking mWattHandTracking;
    // 캡쳐 파일 경로
    String mCaptureFilePath;
    // Camera Zoom Level
    float mZoomLevel = 1.0f;

    // 캡쳐 사진 출력 레이아웃
    @BindView(R.id.layout_capture)
    View layout_capture;
    // 캡쳐 표시 UI
    @BindView(R.id.ivCapture)
    ImageView ivCapture;
    // 뒤로가기 UI
    @BindView(R.id.layout_back)
    View layout_back;
    // 정보 기입 UI
    @BindView(R.id.iclUserRegistrationLayout)
    View iclUserRegistrationLayout;
    // 프리뷰 텍스트
    @BindView(R.id.tvPreviewText)
    TextView tvPreviewText;

    // 체크 리스트 input
    @BindView(R.id.rlCheckListInput)
    RelativeLayout rlCheckListInput;

    // 체크 리스트 조회
    @BindView(R.id.rlCheckListSearch)
    RelativeLayout rlCheckListSearch;

    // 프리뷰 토글
    @BindView(R.id.rlPreviewToggle)
    RelativeLayout rlPreviewToggle;

    // 설정 메인 버튼
    @BindView(R.id.rlSetting)
    RelativeLayout rlSetting;

    // 설정 메인 버튼
    @BindView(R.id.rlBtnImageViewer)
    RelativeLayout rlBtnImageViewer;

    // 설비 명 Input UI
    @BindView(R.id.etDeviceName)
    EditText etDeviceName;

    // 작업 자 이름 Input UI
    @BindView(R.id.etUserName)
    EditText etUserName;

    // 작업 자 이름 Input UI
    @BindView(R.id.tvBtnRegistration)
    TextView tvBtnRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 카메라 프리뷰를  전체화면으로 보여주기 위해 셋팅한다.
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mWattHandTracking = WattHandTracking.getInstance();
        mCaptureFilePath = getFilesDir() + "/capture.jpg";
        initUI();
    }

    /**
     * UI Id Mapping
     */
    void initUI() {
        layout_capture.setVisibility(View.GONE);
        layout_back.setVisibility(View.GONE);
    }

    /**
     * AI UI Listener
     */
    ARHandsComponentHelper.AiUIListener aiClickListener = new ARHandsComponentHelper.AiUIListener() {
        @Override
        public void setAIListener() {
            mWattHandTracking.setAiClickListener(layout_back);
            mWattHandTracking.setAiClickListener(rlCheckListInput);
            mWattHandTracking.setAiClickListener(rlCheckListSearch);
            mWattHandTracking.setAiClickListener(rlSetting);
            mWattHandTracking.setAiClickListener(rlBtnImageViewer);
            mWattHandTracking.setAiClickListener(rlPreviewToggle);
        }

        @Override
        public void setScrollListener() {
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (mWattHandTracking.checkViewChanges(findViewById(R.id.hand_draw_view)))
            ARHandsComponentHelper.startHand(this, getWindow().getDecorView(), mHandGestureListener, aiClickListener);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CAMERA.INTENT_GET_CAMERA_SETTINGS);
        registerReceiver(mCameraBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mCameraBroadcastReceiver);
        MDEBUG.d("# Main onPause");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick({R.id.layout_back, R.id.rlPreviewToggle, R.id.rlSetting, R.id.rlCheckListInput, R.id.rlCheckListSearch, R.id.tvBtnRegistration, R.id.rlBtnImageViewer})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_back:
                backClickProcess();
                break;
            case R.id.rlPreviewToggle:
                if (mWattHandTracking.tooglePreViewVisible()) {
                    tvPreviewText.setText("프리뷰 숨김");
                    iclUserRegistrationLayout.setVisibility(View.GONE);
                }
                else {
                    tvPreviewText.setText("프리뷰 표시");
                    iclUserRegistrationLayout.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.rlCheckListInput: {
                Intent intent = new Intent(MainActivity.this, CheckListInputActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.rlCheckListSearch: {
                Intent intent = new Intent(MainActivity.this, CheckListResultActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.rlSetting: {
                Toast.makeText(getApplicationContext(), "설정 클릭", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.tvBtnRegistration:
                // 작업자 정보 등록 버튼
                String deviceName = etDeviceName.getText().toString();
                String userName = etUserName.getText().toString();
                break;
            case R.id.rlBtnImageViewer:
                // 이미지 뷰어 버튼 클릭
                CustomToast.makeText(this, "이미지 뷰어 클릭.",
                        CustomToast.LONG, CustomToast.SUCCESS, true).show();
                Intent intent = new Intent(MainActivity.this, ImageViewerActivity.class);
                startActivity(intent);
                break;
        }
    }

    WattHandTracking.HandGestureListener mHandGestureListener = new WattHandTracking.HandGestureListener() {
        @Override
        public void move(int moveValue, float scrollX, float scrollY, View view) {
            switch (moveValue) {
                case FINGER.MOVE_LEFT:
                    runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "왼쪽으로 이동", Toast.LENGTH_LONG).show();
                    });
                    break;
                case FINGER.MOVE_RIGHT:
                    runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "오른쪽 이동", Toast.LENGTH_LONG).show();
                    });
                    break;
                case FINGER.MOVE_TOP:
                    runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "위쪽 이동", Toast.LENGTH_LONG).show();
                    });
                    break;
                case FINGER.MOVE_DOWN:
                    runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "아래쪽 이동", Toast.LENGTH_LONG).show();
                    });
                    break;
            }
        }

        @Override
        public void zoom(double value) {
        }

        @Override
        public void getFingerPoint(Point[] fingerPoint) {

        }
    };
    /**
     * Image Capture
     */
    private void captureImage() {
        File file = new File(mCaptureFilePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ImageCapture.OutputFileOptions outputFileOptions =
                new ImageCapture.OutputFileOptions.Builder(file).build();

        mWattHandTracking.mCameraInput.getCameraXPreviewHelper().imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(MainActivity.this),
                new ImageCapture.OnImageSavedCallback() {
                    @Override
                    public void onImageSaved(ImageCapture.OutputFileResults outputFileResults) {
                        // insert your code here.
                        MDEBUG.d("## file save ok");

                        Toast.makeText(getApplicationContext(), "캡쳐 성공", Toast.LENGTH_LONG).show();
                        layout_capture.setVisibility(View.VISIBLE);
                        layout_back.setVisibility(View.VISIBLE);

                        Bitmap bmp = BitmapFactory.decodeFile(mCaptureFilePath);
                        if (bmp != null) {
                            ivCapture.setImageBitmap(bmp);
                        }
                    }

                    @Override
                    public void onError(ImageCaptureException error) {
                        // insert your code here.
                        MDEBUG.d(String.valueOf(error));
                        Toast.makeText(getApplicationContext(), "캡쳐 실패", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    /**
     * 뒤로 가기 버튼 클릭 처리
     *
     * @Comment - 출력되고 있는 UI 제거 후 기본 화면으로 전환
     */
    private void backClickProcess() {
        layout_back.setVisibility(View.GONE);
        layout_capture.setVisibility(View.GONE);
    }

    /**
     * Camera Set Broad Cast
     */
    BroadcastReceiver mCameraBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            if (intent.getAction() == CAMERA.INTENT_GET_CAMERA_SETTINGS) {
                MDEBUG.d("# onReceive");
                String sensor = intent.getStringExtra(CAMERA.EXTRA_SENSOR);
                String eis = intent.getStringExtra(CAMERA.EXTRA_EIS);
                String fov = intent.getStringExtra(CAMERA.EXTRA_FOV);

                mWattHandTracking.stopHands();
                mWattHandTracking.startHands();
            }
        }
    };

    //  기본 옵션
    private void setNormal() {
        MDEBUG.d("Noah setNormal");
        Intent intent = new Intent(CAMERA.INTENT_SET_CAMERA_SETTINGS);
        ComponentName componentName = new ComponentName(CAMERA.ANDROID_SETTINGS_PKG
                , CAMERA.RW_CAMERA_SET_RECEIVER);
        intent.setComponent(componentName);
        intent.putExtra(CAMERA.EXTRA_SENSOR, "binning");
        intent.putExtra(CAMERA.EXTRA_EIS, "full");
        intent.putExtra(CAMERA.EXTRA_FOV, "on");
        sendBroadcast(intent);
        requestCurrentCameraSettings();
    }

    //  화면 좁게
    private void setFovON() {
        MDEBUG.d("## setFovON");
        Intent intent = new Intent(CAMERA.INTENT_SET_CAMERA_SETTINGS);
        intent.putExtra(CAMERA.EXTRA_FOV, "on");
        ComponentName componentName = new ComponentName(CAMERA.ANDROID_SETTINGS_PKG
                , CAMERA.RW_CAMERA_SET_RECEIVER);
        intent.setComponent(componentName);
        sendBroadcast(intent);
        requestCurrentCameraSettings();
    }

    //  화면 넓게
    private void setFovOFF() {
        MDEBUG.d("## setFovOFF");
        Intent intent = new Intent(CAMERA.INTENT_SET_CAMERA_SETTINGS);
        intent.putExtra(CAMERA.EXTRA_FOV, "off");
        ComponentName componentName = new ComponentName(CAMERA.ANDROID_SETTINGS_PKG
                , CAMERA.RW_CAMERA_SET_RECEIVER);
        intent.setComponent(componentName);
        sendBroadcast(intent);
        requestCurrentCameraSettings();
    }

    private void requestCurrentCameraSettings() {
        Intent intent = new Intent(CAMERA.INTENT_GET_CAMERA_SETTINGS);
        ComponentName componentName = new ComponentName(CAMERA.ANDROID_SETTINGS_PKG
                , CAMERA.RW_CAMERA_GET_RECEIVER);
        intent.setComponent(componentName);
        sendBroadcast(intent);
    }
}
