package com.example.mymediapipeex;

import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.camera.core.ImageCapture;

import com.example.mymediapipeex.constant.CAMERA;
import com.example.mymediapipeex.constant.FINGER;
import com.example.mymediapipeex.mediapipe.HandsResultGlRenderer;
import com.example.mymediapipeex.ui.CustomToast;
import com.example.mymediapipeex.utils.Coordinate;
import com.example.mymediapipeex.utils.MDEBUG;
import com.example.mymediapipeex.view.HandDrawView;
import com.google.mediapipe.formats.proto.LandmarkProto;
import com.google.mediapipe.solutioncore.CameraInput;
import com.google.mediapipe.solutioncore.SolutionGlSurfaceView;
import com.google.mediapipe.solutions.hands.HandLandmark;
import com.google.mediapipe.solutions.hands.Hands;
import com.google.mediapipe.solutions.hands.HandsOptions;
import com.google.mediapipe.solutions.hands.HandsResult;

import java.util.HashSet;
import java.util.Set;

public class WattHandTracking {
    private static final String TAG = WattHandTracking.class.getSimpleName();

    //Singleton Instance
    private static WattHandTracking mWattHandTracking;
    //호출 하는 Activity
    private Activity mActivity;
    //호출 하는 Root View
    private View mView;

    private Hands hands;
    // Run the pipeline and the model inference on GPU or CPU.
    private static final boolean RUN_ON_GPU = true;


    // Hand Detect Point Draw View
    private HandDrawView mHandDrawView;

    public void setHandDrawView(HandDrawView mHandDrawView) {
        this.mHandDrawView = mHandDrawView;
    }

    public HandDrawView getHandDrawView() {
        return mHandDrawView;
    }

    // Live camera demo UI and camera components.
    public CameraInput mCameraInput;

    public SolutionGlSurfaceView<HandsResult> glSurfaceView;

    // 카메라 프리뷰 레이아웃
    private FrameLayout mPreviewLayout;
    // Media Pipe Playing Current Value
    boolean mIsRunning = false;

    // AI Click 처리 하기 위한 UI 모음
    public Set<View> mAI_ClickUI = new HashSet<>();
    // AI Scroll 처리 하기 위한 UI 모음
    public Set<View> mAI_ScrollUI = new HashSet<>();

    // 연속 클릭 방지 최소 시간
    long mMinClickTerm = 0;
    // 카메라 객체 비동기 Get 하기 위한 Handler
    Handler mHandler = new Handler();
    // 프리뷰 표시 여부 값
    public boolean mIsPreviewVisible = false;
    // 포커스 된 View 객체 -> 마우스 포인트 올려놓은 상태 (UI focus 된 View)
    private View mFocusedView;
    private boolean mIsFocus = false; // View Status == Focus 인지 저장하는 변수
    private boolean mIsClear = false; // View Seleted 지워야 하는지 담는 변수
    // 스크롤 되고 있는 View
    private View mScrollingView;
    // 손가락 좌표 찾아주는 객체
    HandsResultGlRenderer mHandsResultGlRenderer;

    // Singleton Instance Getter
    public static WattHandTracking getInstance() {
        if (mWattHandTracking == null) {
            mWattHandTracking = new WattHandTracking();
        }
        return mWattHandTracking;
    }

    //  생성자
    private WattHandTracking() {
        mHandsResultGlRenderer = new HandsResultGlRenderer(mHandTrackingListener);
    }

    // 제스쳐 리스너 인터페이스 -> 해당 move에 대한 내용 사용자 정의
    HandGestureListener mHandGestureListener;

    /**
     * Hand Gesture Interface
     */
    public interface HandGestureListener {
        /**
         * 이동 제스쳐
         */
        public void move(int moveValue, float scrollX, float scrollY, View view);

        /**
         * Zoom 제스쳐 처리
         *
         * @param value zoom 증감 값
         */
        public void zoom(double value);

        /**
         * 모든 손가락 마디 지점 반환
         * OpenGL 좌표 to 실제 뷰 좌표로 변환 된 값 반환
         */
        public void getFingerPoint(Point[] fingerPoint);

    }

    /**
     * View Initializes
     * 액티비티 및 프레그먼트 전환 시 해당 함수 호출 필요
     */
    public void initHands(Activity activity, View view, HandGestureListener handGestureListener) {
//        mWattHandTracking.initAIClickListener();
        if (mHandDrawView != null) {
            mHandDrawView.mIsDraw = false;
            mHandDrawView.invalidate();
        }
        mWattHandTracking.initAIClickListener();

        this.mActivity = activity;
        this.mView = view;
        stopHands();
        mView.findViewById(R.id.btn_media_pipe_start).setOnClickListener(v -> {
            CustomToast.makeText(mActivity, "모션 인식 감지를 시작 합니다.",
                    CustomToast.LONG, CustomToast.SUCCESS, true).show();
            startHands();
            mView.findViewById(R.id.fabGifStop).setVisibility(View.VISIBLE);
        });
        mView.findViewById(R.id.btn_media_pipe_stop).setOnClickListener(v -> {
            CustomToast.makeText(mActivity, "모션 인식 감지를 중지 합니다.",
                    CustomToast.LONG, CustomToast.SUCCESS, true).show();
            stopHands();
            mView.findViewById(R.id.fabGifStop).setVisibility(View.GONE);
        });
        mPreviewLayout = mView.findViewById(R.id.preview_display_layout);
        this.mHandDrawView = view.findViewById(R.id.hand_draw_view);
        this.mHandDrawView.mUseZoomView = false;
        this.mHandDrawView.mIconColorRevers = false;

        mHandsResultGlRenderer.mIsHandDetecting = false;
        setupStreamingModePipeline();
        mHandGestureListener = handGestureListener;
        startHands();
        mHandDrawView.bringToFront();
    }

    /**
     * View가 바꼈는지 검사하는 함수
     * 액티비티 이동 시 view 바뀌면 다시 initHands 해줘야함.
     * 필요 생명주기에서 return 값에 따라 initHands 호출 여부 결정
     *
     * @param handDrawView - 그리는 View
     * @return 기존과 동일한 뷰
     */
    public boolean checkViewChanges(HandDrawView handDrawView) {
        if (mHandDrawView == handDrawView)
            return false; // View 바뀌지 않음
        else
            return true; // View 바뀜
    }

    ;

    /**
     * Start Camera
     */
    public void startCamera() {
        MDEBUG.d("# start Camera");
        int cameraWidth = 4608, cameraHeight = 3456; // Test 최상 해상도
        if (Build.MODEL.equals("T1100G") || Build.MODEL.equals("T21G")) { // Glass 최상 해상도
            cameraWidth = CAMERA.RESOLUTION_WIDTH;
            cameraHeight = CAMERA.RESOLUTION_HEGIHT;
        }

        // NOTE - Camera Input Start 이전에 image Capture Builder 생성 해줘야 캡쳐 가능
        mCameraInput.getCameraXPreviewHelper().imageCaptureBuilder = new ImageCapture.Builder();

        mCameraInput.start(
                mActivity,
                hands.getGlContext(),
                CameraInput.CameraFacing.BACK,
//                (glSurfaceView.getWidth()),
//                (glSurfaceView.getHeight()));
                cameraWidth,
                cameraHeight);

//        MDEBUG.d("## Build.MODEL) : " + Build.MODEL);
//        MDEBUG.d("## cameraWidth : " + cameraWidth);
//        MDEBUG.d("## cameraHeight : " + cameraHeight);
//        MDEBUG.d("## glSurfaceView.getWidth() : " + glSurfaceView.getWidth());
//        MDEBUG.d("## glSurfaceView.getHeight() : " + glSurfaceView.getHeight());
//        MDEBUG.d("## mPreviewLayout width : " + mPreviewLayout.getWidth());
//        MDEBUG.d("## mPreviewLayout height : " + mPreviewLayout.getHeight());
//        getCameraThread();
    }

    /**
     * Sets up core workflow for streaming mode.
     */
    public void setupStreamingModePipeline() {
        // Initializes a new MediaPipe Hands solution instance in the streaming mode.
        if (hands == null) {    // Note - 해당 내용 Build 시간 오래걸려 단 한번만 호출하도록 처리
            hands =
                    new Hands(
                            mActivity,
                            HandsOptions.builder()
                                    .setStaticImageMode(false)
                                    .setMaxNumHands(1)
                                    .setRunOnGpu(RUN_ON_GPU)
                                    .build());
        }

        hands.setErrorListener((message, e) -> Log.e(TAG, "MediaPipe Hands error:" + message));
        mCameraInput = new CameraInput();
        // Initializes a new Gl surface view with a user-defined HandsResultGlRenderer.
        glSurfaceView =
                new SolutionGlSurfaceView<>(mActivity, hands.getGlContext(), hands.getGlMajorVersion());
        glSurfaceView.setSolutionResultRenderer(mHandsResultGlRenderer);
        glSurfaceView.setRenderInputImage(true);
        hands.setResultListener(
                handsResult -> {
//                    logWristLandmark(handsResult, /*showPixelValues=*/ true);
                    glSurfaceView.setRenderData(handsResult);
                    glSurfaceView.requestRender();
                });

////        // Updates the preview layout.
//        mPreviewLayout.removeAllViewsInLayout();
        mPreviewLayout.addView(glSurfaceView);
    }

    public void stopCurrentPipeline() {
        if (mCameraInput != null) {
            mCameraInput.setNewFrameListener(null);
            mCameraInput.close();
        }

        if (glSurfaceView != null) {
            glSurfaceView.setVisibility(View.GONE);
        }
//        if (hands != null) {
//            hands.close();
//        }
    }

    // 모션 감지 시작 (view, camera, media pipe) run
    public void startHands() {
        mCameraInput.setNewFrameListener(textureFrame -> hands.send(textureFrame));
        // The runnable to start camera after the gl surface view is attached.
        // For video input source, videoInput.start() will be called when the video uri is available.
        glSurfaceView.post(this::startCamera);
        glSurfaceView.setVisibility(View.VISIBLE);
        mHandDrawView.setVisibility(View.VISIBLE);
        mIsRunning = true;
    }

    // 모션 감지 중지 (view, camera, media pipe) stop
    // 해당 함수 호출 시 AI 클릭 리스너 초기 화 됨
    public void stopHands() {
        stopCurrentPipeline();
        mHandsResultGlRenderer.mIsHandDetecting = false;
    }

    public void logWristLandmark(HandsResult result, boolean showPixelValues) {
        if (result.multiHandLandmarks().isEmpty()) {
            return;
        }
        LandmarkProto.NormalizedLandmark wristLandmark =
                result.multiHandLandmarks().get(0).getLandmarkList().get(HandLandmark.WRIST);
        // For Bitmaps, show the pixel values. For texture inputs, show the normalized coordinates.
        if (showPixelValues) {
            int width = result.inputBitmap().getWidth();
            int height = result.inputBitmap().getHeight();
            Log.i(
                    TAG,
                    String.format(
                            "MediaPipe Hand wrist coordinates (pixel values): x=%f, y=%f",
                            wristLandmark.getX() * width, wristLandmark.getY() * height));
        } else {
            Log.i(
                    TAG,
                    String.format(
                            "MediaPipe Hand wrist normalized coordinates (value range: [0, 1]): x=%f, y=%f",
                            wristLandmark.getX(), wristLandmark.getY()));
        }
        if (result.multiHandWorldLandmarks().isEmpty()) {
            return;
        }
        LandmarkProto.Landmark wristWorldLandmark =
                result.multiHandWorldLandmarks().get(0).getLandmarkList().get(HandLandmark.WRIST);
        Log.i(
                TAG,
                String.format(
                        "MediaPipe Hand wrist world coordinates (in meters with the origin at the hand's"
                                + " approximate geometric center): x=%f m, y=%f m, z=%f m",
                        wristWorldLandmark.getX(), wristWorldLandmark.getY(), wristWorldLandmark.getZ()));
    }

    /**
     * Hand Tracking Listener
     */
    HandsResultGlRenderer.HandTrackingListener mHandTrackingListener = new HandsResultGlRenderer.HandTrackingListener() {
        @Override
        public void detectHand(boolean isDetect) {
            mHandDrawView.mIsDraw = isDetect;
            if (!isDetect) {
                mHandDrawView.invalidate(); // 화면에서 사라진 경우 -> view 없앤 상태 draw
                if (mIsFocus && mIsClear) clearPointFocusView(); // 포커스 지워야 하는지 검사 후 clear
            }
        }

        @Override
        public void getFingerPoint(Point[] fingerPoint) {
            // 좌표 보정 로직
//            for (int i = 0; i < fingerPoint.length; i++) {
//                fingerPoint[i].x = (fingerPoint[i].x + mHandDrawView.mFingerPoint[i].x) / 2;
//                fingerPoint[i].y = (fingerPoint[i].y + mHandDrawView.mFingerPoint[i].y) / 2;
//            }
            if (mIsFocus) mIsClear = true;
            mHandDrawView.setFingerPoint(fingerPoint);
            // 새로 디텍팅 된 마우스 포인트
            Point newMousePoint = new Point((fingerPoint[FINGER.MIDDLE_FINGER_1].x * 3 + fingerPoint[FINGER.WRIST].x) / 4, (fingerPoint[FINGER.MIDDLE_FINGER_1].y * 3 + fingerPoint[FINGER.WRIST].y) / 4);

/*            // NOTE - 20230221 - 해당 로직 임시 제거
            // 마우스 이동 거리가 작은 경우 보정 로직
            if (Math.abs(newMousePoint.x - mHandDrawView.mPreMousePoint.x) < 30 && Math.abs(newMousePoint.y - mHandDrawView.mPreMousePoint.y) < 30) { // 마우스 많이 이동하지 않도록 (떨림 보정) 처리
                // NOTE - * 3, / 4 == midle finger 1 쪽으로 가깝게 표시하기 위한 계산 공식
                mHandDrawView.mPreMousePoint.x = (mHandDrawView.mPreMousePoint.x + (fingerPoint[FINGER.MIDDLE_FINGER_1].x * 3 + fingerPoint[FINGER.WRIST].x) / 4) / 2;
                mHandDrawView.mPreMousePoint.y = (mHandDrawView.mPreMousePoint.y + (fingerPoint[FINGER.MIDDLE_FINGER_1].y * 3 + fingerPoint[FINGER.WRIST].y) / 4) / 2;
            } else {
                mHandDrawView.mPreMousePoint.x = (fingerPoint[FINGER.MIDDLE_FINGER_1].x * 3 + fingerPoint[FINGER.WRIST].x) / 4;
                mHandDrawView.mPreMousePoint.y = (fingerPoint[FINGER.MIDDLE_FINGER_1].y * 3 + fingerPoint[FINGER.WRIST].y) / 4;
            }*/
            // 마우스 포인트 Set
            mHandDrawView.mPreMousePoint.x = (fingerPoint[FINGER.MIDDLE_FINGER_1].x * 3 + fingerPoint[FINGER.WRIST].x) / 4;
            mHandDrawView.mPreMousePoint.y = (fingerPoint[FINGER.MIDDLE_FINGER_1].y * 3 + fingerPoint[FINGER.WRIST].y) / 4;

            if (!checkZoomGesture(fingerPoint) || !mHandDrawView.mUseZoomView || !mHandsResultGlRenderer.mIsBackHand)
                checkClickEvent(fingerPoint[FINGER.THUMB_4], fingerPoint[FINGER.INDEX_FINGER_4], mHandDrawView.mPreMousePoint);
            if (mHandGestureListener != null) mHandGestureListener.getFingerPoint(fingerPoint);
            if (mIsFocus && mIsClear) clearPointFocusView(); // 포커스 지워야 하는지 검사 후 clear
            mHandDrawView.invalidate();
        }

        @Override
        public int getViewWidth() {
            return mHandDrawView.getWidth();
        }

        @Override
        public int getViewHegiht() {
            return mHandDrawView.getHeight();
        }
    };

    /**
     * clickFinger1과 clickFinger2 둘 사이의 거리 가까워지는 경우 클릭 트리거 발생
     * 이 때 클릭 처리 좌표는 pointFinger 위치해 있는 좌표
     *
     * @param clickFinger1 - 제스쳐 손가락 1
     * @param clickFinger2 - 제스쳐 손가락 2
     * @param pointFinger  - 마우스 포인터 손가락
     */
    private void checkClickEvent(Point clickFinger1, Point clickFinger2, Point pointFinger) {
        // 엄지와 검지 사이의 거리
        double thumb_distance = Coordinate.getDistance(clickFinger1.x, clickFinger1.y, clickFinger2.x, clickFinger2.y);
        // 클릭 판단되는 기준 거리 - 해당 값 보다 작은 경우 클릭으로 판단 (원근감에 따른 값 증감)
        double clickCompareDistance = mHandsResultGlRenderer.mFingerSize / FINGER.CLICK_DISTANCE_DIVIDE_VAULE;

        // 이미 클릭 중인 상태 - 거리 멀어지면 클릭 이미지 해제
        if (mHandDrawView.mOnClickGesture || mHandDrawView.mClickdetectCount != 0) {
            // 사이 간격 멀어지면 클릭 해제
            if (thumb_distance > clickCompareDistance) {
                if (++mHandDrawView.mClickdetectCount >= FINGER.CLICK_GESTURE_DETECT_COUNT) {
                    if (!mHandDrawView.mIsScrollGesture) // 제스쳐 아닌 경우에 클릭 처리
                        findViewClickPoint(pointFinger.x, pointFinger.y, true); // 클릭 이벤트
                    mHandDrawView.mOnClickGesture = false;
                    mHandDrawView.mIsScrollGesture = false;
                    mHandDrawView.mClickdetectCount = 0;
                }
            } else {
                if (((mAI_ScrollUI.size() <= 0 || mScrollingView == null) && !mHandDrawView.mUseZoomView)
                        || !checkScrollGesture()) // 스크롤 제스쳐 아닌 경우 아래 포커싱 처리
                    findViewClickPoint(pointFinger.x, pointFinger.y, false); // 포커스 이벤트 처리
                mHandDrawView.mClickdetectCount = 0;
            }
        } else { // 클릭 중이지 않은 상태 - 거리 가까우면 클릭 이미지 및 클릭 이벤트 발생
            // 사이 간격 작은 경우에만 클릭으로 처리
            if (thumb_distance < clickCompareDistance) {
                // 클릭 한 지점 스크롤 포인트 지정
                mHandDrawView.mIsScrollGesture = false;
                mHandDrawView.mScrollStandard.x = mHandDrawView.mPreMousePoint.x;
                mHandDrawView.mScrollStandard.y = mHandDrawView.mPreMousePoint.y;
                mHandDrawView.mOnClickGesture = true;
                mScrollingView = checkScrollViewArea(); // 스크롤 View 안에서 스크롤이 시작되었는지 검사 return == null -> scroll 처리 하지 않음
            }
        }
    }

    /**
     * Zoom 제스쳐 검사
     * 손등인 경우 및 검지와 엄지를 제외한 손가락이 모두 접힌 경우
     *
     * @param fingerPoint
     * @return - 제스쳐 여부
     */
    private boolean checkZoomGesture(Point[] fingerPoint) {
        // 손등이면서 zoom 제스쳐 상태가 아닌 경우
        if (mHandsResultGlRenderer.mIsBackHand && mHandDrawView.mZoomGestureUndetectCount <= FINGER.ZOOM_GESTURE_UNDETECT_COUNT) {
            MDEBUG.d("## mIsBackHand !");
            return false; // 손등 인 경우 false 반환
        }
        boolean[] fingerFoldStatus = mWattHandTracking.fingerFoldCheck(fingerPoint);
        // Zoom Gesture 검사
        boolean isZoomGesture = !fingerFoldStatus[0] && !fingerFoldStatus[1] && fingerFoldStatus[2] && fingerFoldStatus[3] && fingerFoldStatus[4];
        if (mHandDrawView.mZoomGestureUndetectCount <= FINGER.ZOOM_GESTURE_UNDETECT_COUNT) { // 현재 줌 컨트롤 상태가 아닌 경우 제스쳐 검사
            if (isZoomGesture) {
//                mHandDrawView.preZoomDistance = Coordinate.getDistance(fingerPoint[FINGER.INDEX_FINGER_4].x, fingerPoint[FINGER.INDEX_FINGER_4].y,
//                        fingerPoint[FINGER.THUMB_4].x, fingerPoint[FINGER.THUMB_4].y);
                mHandDrawView.mZoomGestureUndetectCount = 0;
            }
        } else { // 줌 컨트롤 상태인 경우
            if (isZoomGesture) { // 제스쳐 진행 중인 상태
                mHandDrawView.mZoomGestureUndetectCount = 0;
                if (mHandGestureListener != null) {
                    // 엄지와 검지 사이의 거리
                    double thumb_distance = Coordinate.getDistance(fingerPoint[FINGER.THUMB_4].x, fingerPoint[FINGER.THUMB_4].y,
                            fingerPoint[FINGER.INDEX_FINGER_4].x, fingerPoint[FINGER.INDEX_FINGER_4].y);
                    // 클릭 판단되는 기준 거리 - 해당 값 보다 작은 경우 클릭으로 판단 (원근감에 따른 값 증감)
                    double clickCompareDistance = mHandsResultGlRenderer.mFingerSize / FINGER.CLICK_DISTANCE_DIVIDE_VAULE;
                    // 기준 거리와 비교하여 zoom in or zoom out 판단
                    double moveDistance = clickCompareDistance - thumb_distance;
                    if (mHandGestureListener != null)
                        mHandGestureListener.zoom(moveDistance);
                }
            } else
                // Zoom 제스쳐 상태에서 풀린 경우
                mHandDrawView.mZoomGestureUndetectCount--;
        }
        return isZoomGesture;
    }

    /**
     * scroll 제스쳐 검사
     *
     * @return 스크롤 중인지 판단 값
     */
    private boolean checkScrollGesture() {
        if (mHandDrawView.mOnClickGesture) {
            float xMoveDistance = mHandDrawView.mScrollStandard.x - mHandDrawView.mPreMousePoint.x;
            float yMoveDistance = mHandDrawView.mScrollStandard.y - mHandDrawView.mPreMousePoint.y;
            if (Math.abs(xMoveDistance) < 50) xMoveDistance = 0; // 중간에 치우친 값 scroll 하지 않음.
            if (Math.abs(yMoveDistance) < 50) yMoveDistance = 0; // 중간에 치우친 값 scroll 하지 않음.
            if (xMoveDistance != 0 || yMoveDistance != 0) {

                if (mHandGestureListener != null)
                    mHandGestureListener.move(moveCheck(xMoveDistance, yMoveDistance),
                            xMoveDistance / 5, yMoveDistance / 5, mScrollingView);
                mHandDrawView.mIsScrollGesture = true;
            }
        }
        return mHandDrawView.mIsScrollGesture;
    }

    /**
     * Move 시작 시점부터 이동 범위 계산
     *
     * @param xMoveDistance x 축 이동 범위
     * @param yMoveDistance y 축 이동 범위
     * @return 이동 내용
     */
    private int moveCheck(float xMoveDistance, float yMoveDistance) {
//        MDEBUG.d("xMoveDistance : " + xMoveDistance);
//        MDEBUG.d("yMoveDistance : " + yMoveDistance);
        int moveValue = FINGER.MOVE_NON;
        if (xMoveDistance > (mHandDrawView.getWidth() / 4)) { // 좌로 이동
            moveValue = FINGER.MOVE_LEFT;
        } else if (xMoveDistance < ((mHandDrawView.getWidth() / 4) * -1)) { // 우로 이동
            moveValue = FINGER.MOVE_RIGHT;
        }
        if (yMoveDistance > (mHandDrawView.getHeight() / 4)) { // 위로 이동
            moveValue = FINGER.MOVE_TOP;
        } else if (yMoveDistance < ((mHandDrawView.getHeight() / 4) * -1)) { // 아래로 이동
            moveValue = FINGER.MOVE_DOWN;
        }
//        if (moveValue != FINGER.MOVE_NON) {
//            mHandDrawView.preHandPosX = -1;
//            mHandDrawView.preHandPosY = -1;
//        }
        return moveValue;
    }

    /**
     * 스크롤 곳 View에 스크롤 뷰 영역에 포함되는지 검사
     *
     * @return 스크롤 한 뷰 ** Null = 스크롤 한 View에 포함 되지 않음
     */
    private View checkScrollViewArea() {
        for (View v : mAI_ScrollUI) {
            int location[] = new int[2]; // x, y 절대 좌표
            v.getLocationInWindow(location);
            int viewXPos = location[0];
            int viewYPos = location[1];

            if (viewXPos <= mWattHandTracking.getHandDrawView().mScrollStandard.x && (viewXPos + v.getWidth()) >= mWattHandTracking.getHandDrawView().mScrollStandard.x
                    && viewYPos <= mWattHandTracking.getHandDrawView().mScrollStandard.y && ((viewYPos + v.getHeight()) >= mWattHandTracking.getHandDrawView().mScrollStandard.y)) { // 클릭한 좌표 값이 해당 View 좌표안에 포함되는지 검사
                return v;
            }
        }
        return null;
    }

    /**
     * 클릭 이벤트 발생 한 좌표에 View 위치하는지 검사 후 클릭 이벤트 발생
     *
     * @param clickX  클릭 한 X 좌표
     * @param clickY  클릭 한 Y 좌표
     * @param isClick true - click event, false - just focus
     */
    private void findViewClickPoint(int clickX, int clickY, boolean isClick) {
        if (SystemClock.elapsedRealtime() - mMinClickTerm < 600) return; // 연속 클릭 최소 시간 처리
        // 버튼 Area 클릭 여부 확인
        for (View v : mAI_ClickUI) {
            int location[] = new int[2]; // x, y 절대 좌표
            v.getLocationInWindow(location);
            int xPos = location[0];
            int yPos = location[1];

            if (xPos <= clickX && (xPos + v.getWidth()) >= clickX
                    && yPos <= clickY && ((yPos + v.getHeight()) >= clickY)) { // 클릭한 좌표 값이 해당 View 좌표안에 포함되는지 검사
                if (isClick) { // 클릭 이벤트
                    mActivity.runOnUiThread(() -> v.performClick());
                    mMinClickTerm = SystemClock.elapsedRealtime(); // 연속 클릭 방지
                } else { // Mouse 포커스 이벤트
                    if (!mIsFocus) { // 이전에 포커스 시키지 않은 경우 -> 현재 좌표에 있는 View Focus
                        mFocusedView = v; // 이후에 지우기 위해 현재 View 저장
                        v.setAlpha(0.2f);
                        mIsFocus = true; // 현재 포커스 된 상태라는 것을 담음
                    } else {
                        if (mFocusedView != null && v == mFocusedView)
                            mIsClear = false; // 현재 포커스 된 View와 이전에 된 View 값이 같은 경우 지우지 않음
                        else // 이전과 다른 View가 포커스 되었다면 포커시 지움
                            mIsClear = true;
                    }
                }
                break;
            }
        }
    }

    /**
     * 손가락 접힘 검사 함수 (엄지 접힘 여부는 다른 방식으로 검사)
     *
     * @param fingerPoint
     * @return 1~5번쨰 손가락 접힘 여부 bool 값
     */
    public boolean[] fingerFoldCheck(Point[] fingerPoint) {
        boolean[] foldCheck = new boolean[5];
        // 엄지 접힘 검사 (새끼1 기준으로 엄지4 와 엄지2거리 비교하여 판단
        double thumbDis4 = Coordinate.getDistance(fingerPoint[FINGER.PINKY_1].x, fingerPoint[FINGER.PINKY_1].y,
                fingerPoint[FINGER.THUMB_4].x, fingerPoint[FINGER.THUMB_4].y);
        double thumbDis2 = Coordinate.getDistance(fingerPoint[FINGER.PINKY_1].x, fingerPoint[FINGER.PINKY_1].y,
                fingerPoint[FINGER.THUMB_2].x, fingerPoint[FINGER.THUMB_2].y);
        if (thumbDis4 < thumbDis2)
            foldCheck[0] = true;
//        MDEBUG.d(0 + "번째 손가락 접힘 여부 : " + foldCheck[0]);
        // 엄지 제외 접힘 검사
        for (int i = 1; i < foldCheck.length; i++) {
            double finger4Dis = Coordinate.getDistance(fingerPoint[FINGER.WRIST].x, fingerPoint[FINGER.WRIST].y,
                    fingerPoint[i * 4 + 3 + 1].x, fingerPoint[i * 4 + 3 + 1].y);
            double finger1Dis = Coordinate.getDistance(fingerPoint[FINGER.WRIST].x, fingerPoint[FINGER.WRIST].y,
                    fingerPoint[i * 4 + 1].x, fingerPoint[i * 4 + 1].y);
            if (finger1Dis > finger4Dis - finger1Dis / 3)
                foldCheck[i] = true;
//            MDEBUG.d(i + "번째 손가락 접힘 여부 : " + foldCheck[i]);
        }
        return foldCheck;
    }

    /**
     * 마우스 포인트 된 View 포커스 해제
     */
    private void clearPointFocusView() {
        mFocusedView.setAlpha(1.0f);
        mIsClear = false;
        mIsFocus = false;
    }

    /**
     * AI Event Click Listener
     */
    public void setAiClickListener(View v) {
        mAI_ClickUI.add(v);
    }

    /**
     * AI Event Scroll Listener
     */
    public void setAiScrollUIListener(View v) {
        mAI_ScrollUI.add(v);
    }

    /**
     * AI 클릭 이벤트 리스너 초기화
     */
    public void initAIClickListener() {
        MDEBUG.d("# initAIClickListener");
        mAI_ClickUI.clear();
        mAI_ScrollUI.clear();
    }

    /**
     * 카메라 프리뷰 표시 Toogle
     */
    public boolean tooglePreViewVisible() {
        mIsPreviewVisible = !mIsPreviewVisible;
        if (mIsPreviewVisible) { // 프리뷰 표시
            mPreviewLayout.setVisibility(View.VISIBLE);
        } else { // 프리뷰 숨김
            mPreviewLayout.setVisibility(View.INVISIBLE);
        }
        return mIsPreviewVisible;
    }

    /**
     * Zoom 사용하는 View 해당 함수 호출
     * Defalut - false
     *
     * @param useZoom
     */
    public void setZoomEnable(boolean useZoom) {
        mHandDrawView.mUseZoomView = useZoom;
    }

    /**
     * 제스쳐 Icon 컬러 반전
     * @param colorReverse - true : 반전 , false : default
     */
    public void setGestureIconColorReverse(boolean colorReverse) {
        mHandDrawView.mIconColorRevers = colorReverse;
    }
}
