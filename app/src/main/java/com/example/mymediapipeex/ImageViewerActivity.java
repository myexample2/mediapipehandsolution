package com.example.mymediapipeex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.mymediapipeex.constant.CAMERA;
import com.example.mymediapipeex.constant.FINGER;
import com.example.mymediapipeex.utils.ARHandsComponentHelper;
import com.peng.plant.wattviewer.ImageControll;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageViewerActivity extends AppCompatActivity {

    // 핸드 트래킹 처리 객체
    WattHandTracking mWattHandTracking;

    // 이미지 뷰어 레이아웃
    @BindView(R.id.rlImageViewer)
    ImageControll rlImageViewer;

    ImageControll mImageControll;

    // 현재 zoom level
    float mCurZoomLevel = 1.0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 카메라 프리뷰를  전체화면으로 보여주기 위해 셋팅한다.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_image_viewer);
        ButterKnife.bind(this);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWattHandTracking = WattHandTracking.getInstance();
        if (mWattHandTracking.checkViewChanges(findViewById(R.id.hand_draw_view)))
            ARHandsComponentHelper.startHand(this, getWindow().getDecorView(), mHandGestureListener, aiClickListener);
        mWattHandTracking.setZoomEnable(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initUI() {
        Bitmap bigPictureBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test_image1);
        mImageControll = new ImageControll(this);
        mImageControll.setImage(bigPictureBitmap);
        mImageControll.showImage(rlImageViewer);
    }

    /**
     * AI UI Listener
     */
    ARHandsComponentHelper.AiUIListener aiClickListener = new ARHandsComponentHelper.AiUIListener() {
        @Override
        public void setAIListener() {

        }

        @Override
        public void setScrollListener() {
        }
    };

    WattHandTracking.HandGestureListener mHandGestureListener = new WattHandTracking.HandGestureListener() {
        @Override
        public void move(int moveValue, float scrollX, float scrollY, View view) {
            runOnUiThread(() -> {
                mImageControll.zoomViewer.Move_Sensor((scrollX * 2) * -1, (scrollY * 2) * -1);
            });
        }

        @Override
        public void zoom(double distanceDiff) {
            float zoomLevel = mCurZoomLevel + (distanceDiff > 0 ? 0.2f : -0.2f); // 매개변수 거리 증감 값에 따른 zoom level 확대 및 감소
            if (zoomLevel > 10f || zoomLevel <= 1.0f)   return; // 최대 zoom 1 ~ 10 으로 제한
            if (zoomLevel >= 1.1f && zoomLevel < 3.0f) zoomLevel = 3.0f; // zoom level 1.0 ~ 2.9값 이상하게 먹혀서 아래 값 처리
            mCurZoomLevel = zoomLevel;
            mImageControll.zoomViewer.zoomlevelcheck(mCurZoomLevel);
        }

        @Override
        public void getFingerPoint(Point[] fingerPoint) {

        }
    };
}