package com.example.mymediapipeex.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.mymediapipeex.ui.MainActivity;
import com.example.mymediapipeex.ui.PatternActivity;
import com.example.mymediapipeex.utils.MDEBUG;

public class PermissionActivity extends AppCompatActivity {
    private static final String TAG = PermissionActivity.class.getSimpleName();
    // 권한 승인 코드
    private int RESULT_PERMISSIONS = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        Log.e(TAG, "start!!");
        // 안드로이드 6.0 이상 버전에서는 CAMERA 권한 허가를 요청한다.
        requestPermission();
    }

    /**
     * Request permission.
     */
    public void requestPermission() {
        int readExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int accessFineLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int wifiChangePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE);
        int wifiAccessPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if(
                readExternalStoragePermission == PackageManager.PERMISSION_GRANTED
                && writeExternalStoragePermission == PackageManager.PERMISSION_GRANTED
                && accessFineLocationPermission == PackageManager.PERMISSION_GRANTED
                && accessCoarseLocationPermission == PackageManager.PERMISSION_GRANTED
                && wifiChangePermission == PackageManager.PERMISSION_GRANTED
                && wifiAccessPermission == PackageManager.PERMISSION_GRANTED
                && cameraPermission == PackageManager.PERMISSION_GRANTED

        ) { // 모두 동의함
            startFirstActivity();
        } else {
            String[] _permissions = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CHANGE_WIFI_STATE,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.CAMERA,
            };

            ActivityCompat.requestPermissions(this, _permissions, RESULT_PERMISSIONS);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MDEBUG.d("onRequestPermissionsResult Act");

        if(grantResults.length == 0) {
            requestPermission();
        } else {
            if(requestCode == RESULT_PERMISSIONS) {
                boolean isGranted = true;

                for(int i = 0; i < grantResults.length; i++) {
                    MDEBUG.d("permissions[" + i + "] : " + permissions[i] + ", grantResults[" + i + "] : " + grantResults[i]);
                    if(grantResults[i] == PackageManager.PERMISSION_GRANTED) { //동의
                    } else { //거부
                        isGranted = false;
                        break;
                    }
                } //for

                if(isGranted) {
                    startFirstActivity();
                } else {
                    Toast.makeText(getApplicationContext(), "[설정] > [권한] 에서 권한을 허용해야 사용할 수 있습니다.", Toast.LENGTH_SHORT).show();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            finishAffinity();
                            System.runFinalizersOnExit(true);
                            System.exit(0);
                        }
                    }, 1000);
                }
            }
        }
    }

    // 패턴 로그인 액티비티
    ActivityResultLauncher<Intent> patternResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                MDEBUG.d("# result Ok!!");
                if (result.getResultCode() == Activity.RESULT_OK) {
                    MDEBUG.d("# result Ok!!");
                    Intent intent = new Intent(PermissionActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

    /**
     * Start login activity.
     */
    public void startFirstActivity() {
        MDEBUG.d("# startFirstActivity !");
//        patternResultLauncher.launch(new Intent(getApplicationContext(), PatternActivity.class));
        patternResultLauncher.launch(new Intent(getApplicationContext(), MainActivity.class));
    }
}
