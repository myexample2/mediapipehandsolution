// Copyright 2021 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.example.mymediapipeex.mediapipe;

import android.graphics.Point;
import android.opengl.GLES20;

import com.example.mymediapipeex.constant.FINGER;
import com.example.mymediapipeex.utils.Coordinate;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;
import com.google.mediapipe.solutioncore.ResultGlRenderer;
import com.google.mediapipe.solutions.hands.Hands;
import com.google.mediapipe.solutions.hands.HandsResult;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

/**
 * A custom implementation of {@link ResultGlRenderer} to render {@link HandsResult}.
 */
public class HandsResultGlRenderer implements ResultGlRenderer<HandsResult> {
    private static final String TAG = "HandsResultGlRenderer";

    private HandTrackingListener mHandTrackingListener; // 트래킹 리스너 (라이브러리에서 정의)

    // Detect Listener
    public interface HandTrackingListener {
        /**
         * hand 표시 여부
         *
         * @param isDetect
         */
        public void detectHand(boolean isDetect);

//        /**
//         * 원을 드래그해서 움직이면 동작하는 함수이다
//         * circleInfoData (원정보)를 변경시키고 다시 화면을 그리도록함
//         *
//         * @param x         = 최종 x좌표 - 원의 x값이 된다.
//         * @param y         = 최종 y좌표 - 원의 y값이 된다.
//         * @param listIndex = drawnCircleList의 들어있는 인덱스값
//         */
//        @
//        public void moveCircleShape(float x, float y, int listIndex);

//        /**
//         * 엄지 클릭 제스쳐 검사
//         * Index 1 기준으로 거리가 index-2 보다 thumb-4 거리가 짧으면 클릭 이벤트로 처리
//         */
//        public void thumb_click_check(PointF thumb_4, PointF index_1, PointF index_2);

        /**
         * 모든 손가락 마디 지점 반환
         * OpenGL 좌표 to 실제 뷰 좌표로 변환 된 값 반환
         */
        public void getFingerPoint(Point[] fingerPoint);

        /**
         * View Width Getter
         */
        public int getViewWidth();

        /**
         * View Hegiht Getter
         */
        public int getViewHegiht();

    }


    private static final float[] LEFT_HAND_CONNECTION_COLOR = new float[]{0.2f, 1f, 0.2f, 1f};
    private static final float[] RIGHT_HAND_CONNECTION_COLOR = new float[]{1f, 0.2f, 0.2f, 1f};
    private static final float CONNECTION_THICKNESS = 25.0f;
    private static final float[] LEFT_HAND_HOLLOW_CIRCLE_COLOR = new float[]{0.2f, 1f, 0.2f, 1f};
    private static final float[] RIGHT_HAND_HOLLOW_CIRCLE_COLOR = new float[]{1f, 0.2f, 0.2f, 1f};
    private static final float HOLLOW_CIRCLE_RADIUS = 0.01f;
    private static final float[] LEFT_HAND_LANDMARK_COLOR = new float[]{1f, 0.2f, 0.2f, 1f};
    private static final float[] RIGHT_HAND_LANDMARK_COLOR = new float[]{0.2f, 1f, 0.2f, 1f};
    private static final float LANDMARK_RADIUS = 0.008f;
    private static final int NUM_SEGMENTS = 120;
    private static final String VERTEX_SHADER =
            "uniform mat4 uProjectionMatrix;\n"
                    + "attribute vec4 vPosition;\n"
                    + "void main() {\n"
                    + "  gl_Position = uProjectionMatrix * vPosition;\n"
                    + "}";
    private static final String FRAGMENT_SHADER =
            "precision mediump float;\n"
                    + "uniform vec4 uColor;\n"
                    + "void main() {\n"
                    + "  gl_FragColor = uColor;\n"
                    + "}";
    private int program;
    private int positionHandle;
    private int projectionMatrixHandle;
    private int colorHandle;

    // 손 디텍팅 중인지 검사
    public boolean mIsHandDetecting = false;
    // 감지 된 손의 back or front 여부 (손등 or 손바닥)
    public boolean mIsBackHand = true;
    // 손가락 길이 - 해당 값으로 손 원거리 판단 ( 손목 ~ 엄지 2번째 point )
    public double mFingerSize;
    // 생성자
    public HandsResultGlRenderer(HandTrackingListener handTrackingListener) {
        this.mHandTrackingListener = handTrackingListener;
    }

    private int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void setupRendering() {
//        program = GLES20.glCreateProgram();
//        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
//        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);
//        GLES20.glAttachShader(program, vertexShader);
//        GLES20.glAttachShader(program, fragmentShader);
//        GLES20.glLinkProgram(program);
//        positionHandle = GLES20.glGetAttribLocation(program, "vPosition");
//        projectionMatrixHandle = GLES20.glGetUniformLocation(program, "uProjectionMatrix");
//        colorHandle = GLES20.glGetUniformLocation(program, "uColor");
    }

    @Override
    public void renderResult(HandsResult result, float[] projectionMatrix) {
        if (result == null) {
            return;
        }
//        GLES20.glUseProgram(program);
//        GLES20.glUniformMatrix4fv(projectionMatrixHandle, 1, false, projectionMatrix, 0);
//        GLES20.glLineWidth(CONNECTION_THICKNESS);

        int numHands = result.multiHandLandmarks().size();
        if (numHands != 0) {
            boolean isLeftHand = result.multiHandedness().get(0).getLabel().equals("Left");
            List<NormalizedLandmark> fingerPoint = result.multiHandLandmarks().get(0).getLandmarkList();
            // View 좌표 보정 값으로 변환
            Point[] point = new Point[21];
            for (int j = 0; j < fingerPoint.size(); j++) {
                point[j] = new Point();
                point[j].x = (int) (Coordinate.posReflection(
                        Coordinate.zValueReflection(fingerPoint.get(j).getX(), fingerPoint.get(j).getZ()),
                        FINGER.X_REFLECTION_RATIO) * mHandTrackingListener.getViewWidth());
                point[j].y = (int) (Coordinate.posReflection(
                        Coordinate.zValueReflection(fingerPoint.get(j).getY(), fingerPoint.get(j).getZ()),
                        FINGER.Y_REFLECTION_RATIO) * mHandTrackingListener.getViewHegiht());
            }
            mHandTrackingListener.getFingerPoint(point);
            mIsBackHand = checkBackHand(point, !isLeftHand);
            numHands = checkOverDistanceZPos(point) == true ? 0 : 1; // 최대 감지 거리보다 손이 멀리있으면 numHands = 0으로 처리
        }

        // 손이 감지되지 않았거나 거리가 먼 경우 마우스 포인트 지움
        if (numHands == 0) {
            if (mIsHandDetecting) { // 마우스 포인트 보이고 있는 경우에 핸드 감지 안된 경우 -> 마우스 포인트 숨김
                // 마우스 디텍팅 포인트 사라지는 경우 안보이게 설정
                mIsHandDetecting = false;
                mHandTrackingListener.detectHand(mIsHandDetecting);
//                activity.runOnUiThread(() -> {
////                    Toast.makeText(activity, "모션 감지 범위를 벗어났습니다.\n다시 손을 카메라 안으로 위치하여주세요.", Toast.LENGTH_SHORT).show();
//                });
            }
        } else {
//            MDEBUG.d(new BigDecimal(z).toPlainString());
            if (!mIsHandDetecting) { // 마우스 포인트 보이지 않고 있는 경우에 핸드 감지 된 경우 -> 마우스 포인트 표시
                mIsHandDetecting = true;
                mHandTrackingListener.detectHand(mIsHandDetecting);
            }
        }

//      drawConnections(
//          result.multiHandLandmarks().get(i).getLandmarkList(),
//          isLeftHand ? LEFT_HAND_CONNECTION_COLOR : RIGHT_HAND_CONNECTION_COLOR);
//            for (NormalizedLandmark landmark : result.multiHandLandmarks().get(i).getLandmarkList()) {
//         Draws the landmark.
//                drawCircle(
//                        landmark.getX(),
//                        landmark.getY(),
//                        isLeftHand ? LEFT_HAND_LANDMARK_COLOR : RIGHT_HAND_LANDMARK_COLOR);
//         Draws a hollow circle around the landmark.
//                drawHollowCircle(
//                        landmark.getX(),
//                        landmark.getY(),
//                        isLeftHand ? LEFT_HAND_HOLLOW_CIRCLE_COLOR : RIGHT_HAND_HOLLOW_CIRCLE_COLOR);
//            }
    }

    /**
     * 최대 감지 거리 제한
     * 엄지2와 손목 길이 계산으로 처리
     * @return true == 멀리 있는 손 (detect 처리 하지 않음)
     */
    private boolean checkOverDistanceZPos(Point[] point) {
        // 손목과 엄지2번 까지의 거리
        mFingerSize = Coordinate.getDistance(point[FINGER.WRIST].x, point[FINGER.WRIST].y,
                point[FINGER.THUMB_2].x, point[FINGER.THUMB_2].y);
        if (mFingerSize < 100) return true; // 100 보다 작은 경우 최대 감지 거리보다 멀리있다고 판단
        else return false;
    }

    /**
     * Deletes the shader program.
     *
     * <p>This is only necessary if one wants to release the program while keeping the context around.
     */
    public void release() {
        GLES20.glDeleteProgram(program);
    }

    private void drawConnections(List<NormalizedLandmark> handLandmarkList, float[] colorArray) {
        GLES20.glUniform4fv(colorHandle, 1, colorArray, 0);
        for (Hands.Connection c : Hands.HAND_CONNECTIONS) {
            NormalizedLandmark start = handLandmarkList.get(c.start());
            NormalizedLandmark end = handLandmarkList.get(c.end());
            float[] vertex = {start.getX(), start.getY(), end.getX(), end.getY()};
            FloatBuffer vertexBuffer =
                    ByteBuffer.allocateDirect(vertex.length * 4)
                            .order(ByteOrder.nativeOrder())
                            .asFloatBuffer()
                            .put(vertex);
            vertexBuffer.position(0);
            GLES20.glEnableVertexAttribArray(positionHandle);
            GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 0, vertexBuffer);
            GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2);
        }
    }

    private void drawCircle(float x, float y, float[] colorArray) {
        GLES20.glUniform4fv(colorHandle, 1, colorArray, 0);
        int vertexCount = NUM_SEGMENTS + 2;
        float[] vertices = new float[vertexCount * 3];
        vertices[0] = x;
        vertices[1] = y;
        vertices[2] = 0;
        for (int i = 1; i < vertexCount; i++) {
            float angle = 2.0f * i * (float) Math.PI / NUM_SEGMENTS;
            int currentIndex = 3 * i;
            vertices[currentIndex] = x + (float) (LANDMARK_RADIUS * Math.cos(angle));
            vertices[currentIndex + 1] = y + (float) (LANDMARK_RADIUS * Math.sin(angle));
            vertices[currentIndex + 2] = 0;
        }
        FloatBuffer vertexBuffer =
                ByteBuffer.allocateDirect(vertices.length * 4)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer()
                        .put(vertices);
        vertexBuffer.position(0);
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);
    }

    private void drawHollowCircle(float x, float y, float[] colorArray) {
        GLES20.glUniform4fv(colorHandle, 1, colorArray, 0);
        int vertexCount = NUM_SEGMENTS + 1;
        float[] vertices = new float[vertexCount * 3];
        for (int i = 0; i < vertexCount; i++) {
            float angle = 2.0f * i * (float) Math.PI / NUM_SEGMENTS;
            int currentIndex = 3 * i;
            vertices[currentIndex] = x + (float) (HOLLOW_CIRCLE_RADIUS * Math.cos(angle));
            vertices[currentIndex + 1] = y + (float) (HOLLOW_CIRCLE_RADIUS * Math.sin(angle));
            vertices[currentIndex + 2] = 0;
        }
        FloatBuffer vertexBuffer =
                ByteBuffer.allocateDirect(vertices.length * 4)
                        .order(ByteOrder.nativeOrder())
                        .asFloatBuffer()
                        .put(vertices);
        vertexBuffer.position(0);
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
        GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, vertexCount);
    }


    /**
     * 손등, 손바닥 검사
     * @param fingerPoint - 21개 Hand Land Mark 좌표
     * @return - true : 손등, false : 손바닥
     */
    private boolean checkBackHand(Point[] fingerPoint, boolean isLeftHand) {
        // 엄지 x 좌표보다 새끼 x 좌표가 더 작은 경우 -> 왼손 : 손등, 오른 손 : 손바닥
        if (fingerPoint[FINGER.THUMB_1].x > fingerPoint[FINGER.PINKY_1].x)
            return isLeftHand ? true : false;
        else   // 엄지 x 좌표보다 새끼 x 좌표가 더 큰 경우 -> 왼손 : 손등, 오른 손 : 손바닥
            return isLeftHand ? false : true;
    }
}
