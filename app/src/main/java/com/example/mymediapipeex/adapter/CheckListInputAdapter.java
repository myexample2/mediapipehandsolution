package com.example.mymediapipeex.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.mymediapipeex.R;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.constant.LIST_INDEX;
import com.example.mymediapipeex.data.InspectionCheckList;
import com.example.mymediapipeex.utils.MDEBUG;

import java.util.ArrayList;


public class CheckListInputAdapter extends RecyclerView.Adapter<CheckListInputAdapter.ViewHolder> {

    public ArrayList<InspectionCheckList> mList;
    private WattHandTracking mWattHandTracking;
    private Context mContext;

    InPutClick mInPutClick;

    // 키패드 입력 클릭 이벤트
    public interface InPutClick {
        public void inputClick(View view, int position);
    }

    public void setInPutClick(InPutClick inputClick) {
        this.mInPutClick = inputClick;
    }

    public CheckListInputAdapter(Context context, ArrayList<InspectionCheckList> arrayList, WattHandTracking wattHandTracking) {
        MDEBUG.d("### CheckListAdapter");
        this.mContext = context;
        this.mList = arrayList;
        this.mWattHandTracking = wattHandTracking;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.cell_checklist_input, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvCheckListTitle.setText(mList.get(position).mTitleName);
        holder.rbArray[LIST_INDEX.RADIO_INDEX_METOHD_INPUT].setText(mList.get(position).mMethodInputTitle);
        holder.rbArray[LIST_INDEX.RADIO_INDEX_CRITERIA_INPUT].setText(mList.get(position).mCriteriaInputTitle);
        setRadioButtonStatus(holder, position); // 상태 값 처리
        onRadioClickProcess(holder, position); // 상태 값 처리
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCheckListTitle;
        public RadioButton[] rbArray = new RadioButton[10];
        public RadioGroup rg_method, rg_criteria, rg_impact, rg_period;

        public ViewHolder(View itemView) {
            super(itemView);
            rg_method = itemView.findViewById(R.id.rg_method);
            rg_criteria = itemView.findViewById(R.id.rg_criteria);
            rg_impact = itemView.findViewById(R.id.rg_impact);
            rg_period = itemView.findViewById(R.id.rg_period);
            tvCheckListTitle = itemView.findViewById(R.id.tvCheckListTitle);
            rbArray[LIST_INDEX.RADIO_INDEX_METHOD_EYES] = itemView.findViewById(R.id.rb_method_eyes);
            rbArray[LIST_INDEX.RADIO_INDEX_METOHD_GAGE] = itemView.findViewById(R.id.rb_method_gage);
            rbArray[LIST_INDEX.RADIO_INDEX_METOHD_INPUT] = itemView.findViewById(R.id.rb_method_input);
            rbArray[LIST_INDEX.RADIO_INDEX_IMPACT_PRODUCTION] = itemView.findViewById(R.id.rb_impact_production);
            rbArray[LIST_INDEX.RADIO_INDEX_IMPACT_QUALITY] = itemView.findViewById(R.id.rb_impact_quality);
            rbArray[LIST_INDEX.RADIO_INDEX_CRITERIA_MOVEMENT] = itemView.findViewById(R.id.rb_criteria_movement);
            rbArray[LIST_INDEX.RADIO_INDEX_CRITERIA_CONFIRM] = itemView.findViewById(R.id.rb_criteria_confirm);
            rbArray[LIST_INDEX.RADIO_INDEX_CRITERIA_INPUT] = itemView.findViewById(R.id.rb_criteria_input);
            rbArray[LIST_INDEX.RADIO_INDEX_PERIOD_DAILY] = itemView.findViewById(R.id.rb_period_daily);
            rbArray[LIST_INDEX.RADIO_INDEX_PERIOD_WEEKLY] = itemView.findViewById(R.id.rb_period_weekly);

            // loop 순차적으로 id mapping
//            for (int i = 0; i < rbArray.length; i++) {
//                int viewId = mContext.getResources().getIdentifier("rrb_list_" + (i + 1), "id", mContext.getPackageName()); //  TextView id mapping
//                rbArray[i] = itemView.findViewById(viewId);
//            }
        }
    }

    /**
     * 라디오 버튼 선택 값 처리
     * 리스트 갱신 될 때 이전 값으로 라디오 버튼 선택 동기화
     * @param holder - col
     * @param position - list index
     */
    private void setRadioButtonStatus(ViewHolder holder, int position) {
        if (mList.get(position).mMethod.length() <= 0) {
            holder.rg_method.clearCheck();
        } else {
            if (mList.get(position).mMethod.equals("육안")) {
                holder.rg_method.check(R.id.rb_method_eyes);
            } else if (mList.get(position).mMethod.equals("게이지")) {
                holder.rg_method.check(R.id.rb_method_gage);
            } else {
                holder.rg_method.check(R.id.rb_method_input);
            }
        }
        if (mList.get(position).mImpact.length() <= 0) {
            holder.rg_impact.clearCheck();
        } else {
            if (mList.get(position).mImpact.equals("생산")) {
                holder.rg_impact.check(R.id.rb_impact_production);
            } else if (mList.get(position).mImpact.equals("품질")) {
                holder.rg_impact.check(R.id.rb_impact_quality);
            }
        }
        if (mList.get(position).mCriteria.length() <= 0) {
            holder.rg_criteria.clearCheck();
        } else {
            if (mList.get(position).mCriteria.equals("동작")) {
                holder.rg_criteria.check(R.id.rb_criteria_movement);
            } else if (mList.get(position).mCriteria.equals("확인")) {
                holder.rg_criteria.check(R.id.rb_criteria_confirm);
            } else {
                holder.rg_criteria.check(R.id.rb_criteria_input);
            }
        }
        if (mList.get(position).mPeriod.length() <= 0) {
            holder.rg_period.clearCheck();
        } else {
            if (mList.get(position).mPeriod.equals("매일")) {
                holder.rg_period.check(R.id.rb_period_daily);
            } else if (mList.get(position).mPeriod.equals("주간")) {
                holder.rg_period.check(R.id.rb_period_weekly);
            }
        }
    }

    /**
     * 라디오 Click에 대한 처리
     * @param holder - col
     * @param position - list index
     */
    private void onRadioClickProcess(ViewHolder holder, int position) {
        for (RadioButton btn : holder.rbArray) {
            mWattHandTracking.setAiClickListener(btn);
            btn.setOnClickListener(v -> {
                switch (v.getId()) {
                    case R.id.rb_method_eyes:
                        mList.get(position).mMethod = "육안";
                        break;
                    case R.id.rb_method_gage:
                        mList.get(position).mMethod = "게이지";
                        break;
                    case R.id.rb_method_input:
                        mInPutClick.inputClick(v, position);
                        break;
                    case R.id.rb_impact_production:
                        mList.get(position).mImpact = "생산";
                        break;
                    case R.id.rb_impact_quality:
                        mList.get(position).mImpact = "품질";
                        break;
                    case R.id.rb_criteria_movement:
                        mList.get(position).mCriteria = "동작";
                        break;
                    case R.id.rb_criteria_confirm:
                        mList.get(position).mCriteria = "확인";
                        break;
                    case R.id.rb_criteria_input:
                        mInPutClick.inputClick(v, position);
                        break;
                    case R.id.rb_period_daily:
                        mList.get(position).mPeriod = "매일";
                        break;
                    case R.id.rb_period_weekly:
                        mList.get(position).mPeriod = "주간";
                        break;
                }
            });
        }
    }

    public void setSeleted(int _idx, boolean _isSeleted) {
    }
}