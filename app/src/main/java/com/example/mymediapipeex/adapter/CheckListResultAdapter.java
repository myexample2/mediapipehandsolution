package com.example.mymediapipeex.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.mymediapipeex.R;
import com.example.mymediapipeex.WattHandTracking;
import com.example.mymediapipeex.db.DTO_CheckList;
import com.example.mymediapipeex.utils.MDEBUG;

import java.util.List;


public class CheckListResultAdapter extends RecyclerView.Adapter<CheckListResultAdapter.ViewHolder> {
    private List<DTO_CheckList> mList;
    private Context mContext;
    // 항목 Detail or Time List
    public boolean mIsViewDetail = false;

    // 리스트 클릭 이벤트
    private ItemClick itemClick;

    public void setmList(List<DTO_CheckList> mList) {
        this.mList = mList;
    }

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }
    public interface ItemClick {
        public void itemClick(View view, int position);
    }

    public CheckListResultAdapter(Context context, List<DTO_CheckList> arrayList) {
        MDEBUG.d("### CheckListResultAdapter");
        this.mContext = context;
        this.mList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.cell_checklist_result, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) { // 첫 번 째 타이틀 (메뉴) 에만 색상 기입
            holder.ll_detail_list.setBackgroundColor(Color.parseColor("#03A9F4"));
            holder.ll_time_list.setBackgroundColor(Color.parseColor("#03A9F4"));
            holder.tvCheckListTime.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvCheckListUserName.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvCheckListDeviceName.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvCheckListTitle.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvResultMethod.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvResultImpact.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvResultCriteria.setTextColor(Color.parseColor("#ffffffff"));
            holder.tvResultPeriod.setTextColor(Color.parseColor("#ffffffff"));
        }
        if (mIsViewDetail) {
            holder.ll_detail_list.setVisibility(View.VISIBLE);
            holder.ll_time_list.setVisibility(View.GONE);
            holder.tvCheckListTitle.setText(mList.get(position).mTitle);
            holder.tvResultMethod.setText(mList.get(position).mMethod);
            holder.tvResultImpact.setText(mList.get(position).mImpact);
            holder.tvResultCriteria.setText(mList.get(position).mCriteria);
            holder.tvResultPeriod.setText(mList.get(position).mPeriod);
        } else {
            holder.ll_detail_list.setVisibility(View.GONE);
            holder.ll_time_list.setVisibility(View.VISIBLE);
            holder.tvCheckListTime.setText(mList.get(position).mCreateAt);
            holder.tvCheckListUserName.setText(mList.get(position).mUserName);
            holder.tvCheckListDeviceName.setText(mList.get(position).mDeviceName);

            holder.ll_time_list.setOnClickListener(v -> {
                itemClick.itemClick(v, position);
            });
            holder.llCheckListDelete.setOnClickListener(v -> {
                itemClick.itemClick(v, position);
            });
            WattHandTracking.getInstance().setAiClickListener(holder.ll_time_list);
            WattHandTracking.getInstance().setAiClickListener(holder.llCheckListDelete);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCheckListTitle;
        TextView tvResultMethod;
        TextView tvResultImpact;
        TextView tvResultCriteria;
        TextView tvResultPeriod;
        TextView tvCheckListTime;
        TextView tvCheckListUserName;
        TextView tvCheckListDeviceName;

        LinearLayout ll_time_list; // 리스트 표시 레이아웃
        LinearLayout ll_detail_list; // 항목 Detial 표시 레이아웃

        RelativeLayout llCheckListDelete; // 삭제 버튼 레이아웃

        public ViewHolder(View itemView) {
            super(itemView);
            tvCheckListTitle = itemView.findViewById(R.id.tvCheckListTitle);
            tvResultMethod = itemView.findViewById(R.id.tvResultMethod);
            tvResultImpact = itemView.findViewById(R.id.tvResultImpact);
            tvResultCriteria = itemView.findViewById(R.id.tvResultCriteria);
            tvResultPeriod = itemView.findViewById(R.id.tvResultPeriod);
            tvCheckListTime = itemView.findViewById(R.id.tvCheckListTime);
            tvCheckListUserName = itemView.findViewById(R.id.tvCheckListUserName);
            tvCheckListDeviceName = itemView.findViewById(R.id.tvCheckListDeviceName);

            ll_time_list = itemView.findViewById(R.id.ll_time_list);
            ll_detail_list = itemView.findViewById(R.id.ll_detail_list);
            llCheckListDelete = itemView.findViewById(R.id.ll_checklist_delete);
        }
    }

    public void setSeleted(int _idx, boolean _isSeleted) {
    }
}