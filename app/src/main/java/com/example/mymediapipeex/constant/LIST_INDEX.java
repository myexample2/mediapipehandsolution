package com.example.mymediapipeex.constant;

public class LIST_INDEX
{
    /** 점검 항목 라디오 버튼 그룹 인덱스 나열 **/
    public static final int RADIO_INDEX_METHOD_EYES = 0; // 점검방법 - 육안
    public static final int RADIO_INDEX_METOHD_GAGE = 1; // 점검방법 - 게이지
//    public static final int RADIO_INDEX_METOHD_TEST = 2; // 점검방법 - 테스트
    public static final int RADIO_INDEX_METOHD_INPUT = 2; // 점검방법 - 입력

    public static final int RADIO_INDEX_IMPACT_PRODUCTION = 3; // 점검 영향 - 생산
    public static final int RADIO_INDEX_IMPACT_QUALITY = 4; // 점검 영향 - 품질

    public static final int RADIO_INDEX_CRITERIA_MOVEMENT = 5; // 판단 기준 - 동작
    public static final int RADIO_INDEX_CRITERIA_CONFIRM = 6; // 판단 기준 - 확인
    public static final int RADIO_INDEX_CRITERIA_INPUT = 7; // 판단 기준 - 입력

    public static final int RADIO_INDEX_PERIOD_DAILY = 8; //  점검 주기 - 매일
    public static final int RADIO_INDEX_PERIOD_WEEKLY = 9; //  점검 주기 - 주간



}
