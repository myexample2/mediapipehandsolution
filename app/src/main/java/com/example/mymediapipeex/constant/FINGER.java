package com.example.mymediapipeex.constant;

/**
 * Landmark Finger Foint Index
 */
public class FINGER {
    public static final int WRIST = 0;
    public static final int THUMB_1 = 1;
    public static final int THUMB_2 = 2;
    public static final int THUMB_3 = 3;
    public static final int THUMB_4 = 4; //  엄지
    public static final int INDEX_FINGER_1 = 5;
    public static final int INDEX_FINGER_2 = 6;
    public static final int INDEX_FINGER_3 = 7;
    public static final int INDEX_FINGER_4 = 8; // 검지
    public static final int MIDDLE_FINGER_1 = 9;
    public static final int MIDDLE_FINGER_2 = 10;
    public static final int MIDDLE_FINGER_3 = 11;
    public static final int MIDDLE_FINGER_4 = 12; // 중지
    public static final int RING_FINGER_1 = 13;
    public static final int RING_FINGER_2 = 14;
    public static final int RING_FINGER_3 = 15;
    public static final int RING_FINGER_4 = 16; // 약지
    public static final int PINKY_1 = 17;
    public static final int PINKY_2 = 18;
    public static final int PINKY_3 = 19;
    public static final int PINKY_4 = 20; // 새끼

    public static final int CLICK_DISTANCE = 60; // 클릭 판정 거리
    public static final int NON_CLICK_DISTANCE = 80; // 클릭 해제 판정 거리

    public static final int CLICK_ANGLE = 35; // 클릭 판정 거리
    public static final int NON_CLICK_ANGLE = 15; // 클릭 해제 판정 거리

    public static final float X_REFLECTION_RATIO = 2.0f; // X 보정 값 나누기 배율 (값이 낮아야 보정 크게 됨)
    public static final float Y_REFLECTION_RATIO = 1.0f; // Y 보정 값 나누기 배율 (값이 낮아야 보정 크게 됨)

    public static final int MOVE_NON = 0; // 이동 제스쳐 없음
    public static final int MOVE_LEFT = 1; // 왼쪽으로 이동 제스쳐
    public static final int MOVE_RIGHT = 2; // 오른쪽으로 이동 제스쳐
    public static final int MOVE_TOP = 3; // 위로 이동 제스쳐
    public static final int MOVE_DOWN = 4; // 아래로 이동 제스쳐

    public static final double CLICK_DISTANCE_DIVIDE_VAULE = 3.3; // 클릭 제스쳐 판단 거리 Divide 비율 (낮을 수록 클릭 이벤트 발생 높아짐)
    public static final int CLICK_GESTURE_DETECT_COUNT = 2; // 클릭 제스쳐 판단 값
    public static final int ZOOM_GESTURE_UNDETECT_COUNT = -2; // 확대 제스쳐 해제 판단 값 (제스쳐에서 벗어나는 경우 해당 값 보다 낮아야 함) 마이너스 카운트
    public static final int SCROLL_GESTURE_UNDETECT_COUNT = -3; // 스크롤 제스쳐 해제 판단 값     ""
}
