package com.example.mymediapipeex.constant;

public class CAMERA {
    public static final int RESOLUTION_WIDTH = 3840; // 카메라 해상도 Width
    public static final int RESOLUTION_HEGIHT = 2160; // 카메라 해상도 Height

    // Camera 관련 Broad Cast
    public static final String ANDROID_SETTINGS_PKG = "com.android.settings";
    public static final String RW_CAMERA_GET_RECEIVER = "com.android.settings.RealwearCameraGetReceiver";
    public static final String RW_CAMERA_SET_RECEIVER = "com.android.settings.RealwearCameraSetReceiver";
    public static final String INTENT_SET_CAMERA_SETTINGS = "com.android.settings.realwear_camera_SET";
    public static final String INTENT_GET_CAMERA_SETTINGS = "com.android.settings.realwear_camera_GET";

    public static final String EXTRA_SENSOR = "sensor";
    public static final String EXTRA_EIS = "eis";
    public static final String EXTRA_FOV = "fov";

}
