package com.example.mymediapipeex.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.mymediapipeex.R;
import com.example.mymediapipeex.constant.FINGER;
import com.example.mymediapipeex.data.CircleInfoData;
import com.example.mymediapipeex.utils.MDEBUG;

public class HandDrawView extends View {
    //    생성된 원들을 담아놓는 리스트이다
//    원의 정보가 들어가있는 CircleInfoData(원정보) 를 담고있다
//    원이 생성될 시 drawnCircleList에 원들이 추가된다
//    원을 그리기 위해서 onDraw에서 drawnCircleList에서 원의 대한 정보를 가져와 그리도록한다
    private CircleInfoData drawnCircleList = new CircleInfoData();

    private Paint paintRed;
    private Paint paintBlue;
    private Paint mBlackPaint;

    // Click Event Bitmap
    private Bitmap clickBitmap;

    // 점 사이 가까운 경우 line draw
    public boolean mOnClickGesture = false;

    // 포인트 Draw 여부
    public boolean mIsDraw = false;

    // 손바닥 랜드마크 좌표
    public Point[] mFingerPoint;

    // 이전 마우스 포인트 좌표
    public Point mPreMousePoint = new Point();
    // 스크롤 지정 한 기준 Point (해당 좌표 기준으로 scroll)
    public Point mScrollStandard = new Point();
    // Zoom Gesture 처리 위한 pre position value
    public double preZoomDistance = 0;
    // Zoom Gesture 해제 카운트 (해당 제스쳐에서 벗어 날 때 마다 값 감소)
    public int mZoomGestureUndetectCount = FINGER.ZOOM_GESTURE_UNDETECT_COUNT;
    // Scroll Gesture 해제 카운트 (해당 제스쳐에서 벗어 날 때 마다 값 감소)
    public int mScrollGestureUndetectCount = FINGER.SCROLL_GESTURE_UNDETECT_COUNT;
    // Is Scroll Gesture bool
    public boolean mIsScrollGesture = false;
    // Click Gesture 판별 카운트 (해당 제스쳐에 2번 이상 감지 되어야 클릭이벤트로 판정)
    public int mClickdetectCount = 0;
    // View Zoom 기능 사용 여부 - 해당 값에 따라 zoom 아이콘 사용 여부 결정
    public boolean mUseZoomView = false;
    // 제스쳐 아이콘 컬러 반전 (배경 색 검은 색 인 경우 해당 변수 사용)
    public boolean mIconColorRevers = false;
    // Setter
    public void setFingerPoint(Point[] point) {
        this.mFingerPoint = point;
    }

    public HandDrawView(Context context) {
        super(context);
        init();
    }

    public HandDrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HandDrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public HandDrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        paintRed = new Paint();
        paintRed.setAntiAlias(true);
        paintRed.setStyle(Paint.Style.STROKE);
        paintRed.setColor(Color.RED);
        paintRed.setStrokeWidth(5);

        paintBlue = new Paint();
        paintBlue.setAntiAlias(true);
        paintBlue.setStyle(Paint.Style.STROKE);
        paintBlue.setColor(Color.BLUE);
        paintBlue.setStrokeWidth(5);

        mBlackPaint = new Paint();
        mBlackPaint.setStyle(Paint.Style.STROKE);
        mBlackPaint.setColor(Color.parseColor("#80000000"));
//        paintBlue.setColor(Color.GREEN);
        mBlackPaint.setStrokeWidth(5);

        clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.click_event);
    }

    //    그리는 곳
    @Override
    protected void onDraw(Canvas canvas) {
        if (mIsDraw) {
            Bitmap resizeBitmap;
            if (mIsScrollGesture) { // scroll gesture 상태 인 경우
                MDEBUG.d("mIconColorRevers:  " + mIconColorRevers);
                if (mIconColorRevers) // 컬러 반전 비트맵 검사
                    clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.icon_scroll_gesture_revers);
                else
                    clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.icon_scroll_gesture);
                resizeBitmap = Bitmap.createScaledBitmap(clickBitmap, clickBitmap.getWidth() / 10, clickBitmap.getHeight() / 10, true);
            }
            else { // scroll gesture 상태가 아닌 경우
                if (mZoomGestureUndetectCount > FINGER.ZOOM_GESTURE_UNDETECT_COUNT && mUseZoomView) {
                    if (mIconColorRevers) // 컬러 반전 비트맵 검사
                        clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.icon_magnifier_revers);
                    else
                        clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.icon_magnifier);
                    resizeBitmap = Bitmap.createScaledBitmap(clickBitmap, clickBitmap.getWidth() / 6, clickBitmap.getHeight() / 6, true);

                } else {
                    // point click effect draw
                    if (mOnClickGesture) { // 클릭 상태 포인트 이미지
                        clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.mouse_point_click);
                    } else { // 기본 마우스 포인트
                        clickBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.mouse_point);
                    }
                    resizeBitmap = Bitmap.createScaledBitmap(clickBitmap, clickBitmap.getWidth() / 20, clickBitmap.getHeight() / 20, true);

                }
            }
            // 제스쳐 조건 검사로 비트맵 이미지 설정 후 아래 내용으로 drawing
            canvas.drawBitmap(resizeBitmap, mPreMousePoint.x - (resizeBitmap.getWidth() / 2),
                    mPreMousePoint.y - (resizeBitmap.getHeight() / 2),  null);
        }
        super.onDraw(canvas);
    }

    /**
     * 원을 드래그해서 움직이면 동작하는 함수이다
     * circleInfoData (원정보)를 변경시키고 다시 화면을 그리도록함
     *
     * @param x = 최종 x좌표 - 원의 x값이 된다.
     * @param y = 최종 y좌표 - 원의 y값이 된다.
     */
    public void moveMousePoint(float x, float y) {
        drawnCircleList.setCircleX(x);
        drawnCircleList.setCircleY(y);
    }

    /**
     * Hand Draw
     *
     * @param canvas - canvas
     */
    private void drawHand(Canvas canvas) {
        if (mFingerPoint == null) return;
        canvas.scale(0.3f, 0.3f, getWidth() / 2, getHeight() / 2);
        // NOTE - 해당 값 조정 시 손 비율 줄여짐
        float ratio = 1f; // 축소 비율 (손 그림)
        float fingerW = 10f; // 손가락 굵기 (넓이)

        Path leftPath = new Path(); // 손가락 왼쪽 부분 Drawing Path
        Path rightPath = new Path(); // 손가락 오른쪽 부분 Drawing Path
        int index = 1; // 손가락 접근 하기 위한 인덱스
        for (int i = 0; i < 5; i++) { // 다섯 손가락 1~4번째 마디 draw
            /**
             * 좌표 계산 방법 좌표 - (좌표 - 다음 좌표)
             * Y 좌표 구현 필요 **/
            leftPath.moveTo(mFingerPoint[index].x - fingerW, mFingerPoint[index].y);
            rightPath.moveTo(mFingerPoint[index].x + fingerW, mFingerPoint[index].y);
            leftPath.lineTo(mFingerPoint[index].x - fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[index + 1].y) * ratio);
            rightPath.lineTo(mFingerPoint[index].x + fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[++index].y) * ratio);
            leftPath.lineTo(mFingerPoint[index].x - fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[index + 1].y) * ratio);
            rightPath.lineTo(mFingerPoint[index].x + fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[++index].y) * ratio);
            leftPath.lineTo(mFingerPoint[index].x - fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[index + 1].y) * ratio);
            rightPath.lineTo(mFingerPoint[index].x + fingerW, mFingerPoint[index].y - (mFingerPoint[index].y - mFingerPoint[++index].y) * ratio);

            rightPath.lineTo(mFingerPoint[index].x - fingerW, mFingerPoint[index++].y);
        }

        float startVal = 0;
        float fromVal = 0;

        if (Math.abs(mFingerPoint[FINGER.THUMB_1].x - fingerW - mFingerPoint[FINGER.INDEX_FINGER_1].x) >
                Math.abs(mFingerPoint[FINGER.THUMB_1].x + fingerW - mFingerPoint[FINGER.INDEX_FINGER_1].x)) {
            startVal = fingerW;
            fromVal = fingerW * -1;
        } else {
            startVal = fingerW * -1;
            fromVal = fingerW;
        }
        canvas.drawLine(mFingerPoint[FINGER.THUMB_1].x + startVal, mFingerPoint[FINGER.THUMB_1].y,
                mFingerPoint[FINGER.INDEX_FINGER_1].x + fromVal, mFingerPoint[FINGER.INDEX_FINGER_1].y, mBlackPaint);
        canvas.drawLine(mFingerPoint[FINGER.INDEX_FINGER_1].x + startVal, mFingerPoint[FINGER.INDEX_FINGER_1].y,
                mFingerPoint[FINGER.MIDDLE_FINGER_1].x + fromVal, mFingerPoint[FINGER.MIDDLE_FINGER_1].y, mBlackPaint);
        canvas.drawLine(mFingerPoint[FINGER.MIDDLE_FINGER_1].x + startVal, mFingerPoint[FINGER.MIDDLE_FINGER_1].y,
                mFingerPoint[FINGER.RING_FINGER_1].x + fromVal, mFingerPoint[FINGER.RING_FINGER_1].y, mBlackPaint);
        canvas.drawLine(mFingerPoint[FINGER.RING_FINGER_1].x + startVal, mFingerPoint[FINGER.RING_FINGER_1].y,
                mFingerPoint[FINGER.PINKY_1].x + fromVal, mFingerPoint[FINGER.PINKY_1].y, mBlackPaint);
        canvas.drawPath(leftPath, mBlackPaint);
        canvas.drawPath(rightPath, mBlackPaint);
    }
}
