package com.example.mymediapipeex

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.mymediapipeex.utils.MDEBUG
import java.util.*

/**
 * Created by khm on 2021-09-01.
 */

class KeypadNumber(
    private val context: Context,
    private val rootLayout: LinearLayout,
    val keypadDialog: KeypadDialog
) {

    init {
        initUI()
        initKeyboard()
    }

    fun enable(){
        rootLayout.visibility = View.VISIBLE
    }

    fun disable(){
        rootLayout.visibility = View.GONE
    }

    private fun initUI(){
        rootLayout.run {
            findViewById<TextView>(R.id.tv_delete).setOnClickListener {
                subText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_delete))


            findViewById<TextView>(R.id.tv_clear).setOnClickListener {
                EditingText.clearText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_clear))

            findViewById<TextView>(R.id.tv_move_first).setOnClickListener {
                EditingText.cursor.moveFirst()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_first))

            findViewById<TextView>(R.id.tv_move_last).setOnClickListener {
                EditingText.cursor.moveLast()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_last))

            findViewById<TextView>(R.id.tv_move_left).setOnClickListener {
                EditingText.cursor.moveLeft(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left))

            findViewById<TextView>(R.id.tv_move_right).setOnClickListener {
                EditingText.cursor.moveRight(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right))

            findViewById<TextView>(R.id.tv_move_left_word).setOnClickListener {
                EditingText.cursor.moveLeftWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left_word))

            findViewById<TextView>(R.id.tv_move_right_word).setOnClickListener {
                EditingText.cursor.moveRightWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right_word))


        }
    }

    @SuppressLint("SetTextI18n")
    private fun initKeyboard(){
        val lines = getChildArray(rootLayout.findViewById<LinearLayout>(R.id.bottom_layout_number), LinearLayout(context))

        for(i in 0 until lines[0].childCount){
            if(lines[0].getChildAt(i) is TextView){
                val textView = lines[0].getChildAt(i) as TextView
                textView.setOnClickListener{
                    MDEBUG.d("## textView.text.toString() : " + textView.text.toString())
                    inputText(textView.text.toString())
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(textView)
            }
            if(lines[1].getChildAt(i) is TextView){
                val textView = lines[1].getChildAt(i) as TextView
                textView.setOnClickListener{
                    inputText(textView.text.toString())
                    MDEBUG.d("## textView.text.toString() : " + textView.text.toString())
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(textView)
            }
        }

        val optionKeys = ArrayList<TextView>()
        for(i in 0 until lines[2].childCount){
            if(lines[2].getChildAt(i) is TextView){
                optionKeys.add(lines[2].getChildAt(i) as TextView)
            }
        }

        optionKeys[0].text = "취소"
        optionKeys[0].setOnClickListener{
            EditingText.restoreText()
            EditingText.dismissDialog()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(optionKeys[0])


        optionKeys[1].text = "확인"
        optionKeys[1].setOnClickListener{
            EditingText.dismissDialog()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(optionKeys[1])


//        optionKeys[0].text = "한글 전환"
//        optionKeys[0].setOnClickListener{
//            EditingText.currentKeypadType = com.example.mymediapipeex.TypeKeypad.Korean
//        }
//
//        optionKeys[1].text = "영문 전환"
//        optionKeys[1].setOnClickListener{
//            EditingText.currentKeypadType = com.example.mymediapipeex.TypeKeypad.English
//        }
//
//        optionKeys[2].text = "특수 문자"
//        optionKeys[2].setOnClickListener{
//            EditingText.currentKeypadType = com.example.mymediapipeex.TypeKeypad.Special
//        }
//
//        optionKeys[3].text = "취소"
//        optionKeys[3].setOnClickListener{
//            EditingText.restoreText()
//            EditingText.dismissDialog()
//        }
//
//        optionKeys[4].text = "확인"
//        optionKeys[4].setOnClickListener{
//            EditingText.dismissDialog()
//        }

        if(Locale.getDefault() == Locale.KOREA){
            addCommand("영"){
                inputText("0")
            }
        }

    }


    private inline fun <reified T>getChildArray(targetView: ViewGroup, type:T): java.util.ArrayList<T> {
        val array = java.util.ArrayList<T>()

        val countChild = targetView.childCount
        for(i in 0 until countChild){
            if(targetView.getChildAt(i) is T){
                array.add(targetView.getChildAt(i) as T)
            }
        }
        return array
    }


    private fun addCommand(commandText: String, onClickCallback: () -> Unit){
        val textView = TextView(context)
        textView.text = commandText
        textView.contentDescription = "hf_no_number"
        val lp = LinearLayout.LayoutParams(1, 1)
        textView.layoutParams = lp
        textView.setOnClickListener {
            onClickCallback()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
        rootLayout.findViewById<LinearLayout>(R.id.ll_command).addView(textView)
    }

    private fun inputText(str: String){
        addTexts(str)
        EditingText.cursor.moveRight(str.length)
    }


    private fun addTexts(str: String){
        //l.d("addTexts -> str length : ${str.length}")
        val currentText = EditingText.editingText
        if(currentText.isEmpty()){
            //l.e("current text is empty")
            EditingText.editingText = str
        } else{
            val nowPosition = EditingText.nowCursorPosition

            val begin = currentText.substring(0, nowPosition)
            val end = currentText.substring(nowPosition)

            var completeText = begin
            completeText += str
            completeText += end
            EditingText.editingText = completeText
            //l.d("addText after text : $completeText")
        }
    }

    private fun subText(){
        val currentText = EditingText.editingText
        if(currentText.isEmpty())
            return

        var nowPosition = EditingText.nowCursorPosition
        //l.d("before subtext ----- currentText : $currentText, nowPosition : $nowPosition, textLength : ${currentText.length}")

        if(nowPosition > currentText.length){
            //l.d("nowPosition changed --- $nowPosition -> ${currentText.length}")
            nowPosition = currentText.length
        }

        if(nowPosition > 0){
            val completeText = currentText.removeRange(nowPosition - 1, nowPosition)
            EditingText.editingText = completeText
            EditingText.nowCursorPosition = nowPosition-1
        }

        //l.d("after subtext ----- currentText : ${EditingText.editingText}, nowPosition : ${EditingText.nowCursorPosition}, textLength : ${EditingText.editingText.length}")
    }


}