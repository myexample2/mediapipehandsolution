package com.example.mymediapipeex

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Point
import android.util.Log
import android.widget.EditText
import com.example.mymediapipeex.WattHandTracking.HandGestureListener
import com.example.mymediapipeex.utils.ARHandsComponentHelper


@SuppressLint("CutPasteId")
class KeypadDialog(context: Context, activity: Activity) :
    Dialog(context, R.style.DialogCustomTheme) {
    //호출 하는 Activity
    var mActivity: Activity? = activity;

    // 핸드 트래킹 처리 객체
    var mWattHandTracking: WattHandTracking? = null

    private var inputData: EditText? = null

    private var keypadEnglish: KeypadEnglish? = null
    private var keypadKorean: KeypadKorean? = null
    private var keypadSpecial: KeypadSpecial? = null
    private var keypadNumber: KeypadNumber? = null

    init {
        setContentView(R.layout.dialog_keypad)

        inputData = findViewById(R.id.input_data)
        inputData?.run {
            setTextIsSelectable(true)
            showSoftInputOnFocus = false
            requestFocus()
        }
        mWattHandTracking = WattHandTracking.getInstance()

        if (mWattHandTracking!!.checkViewChanges(findViewById(R.id.hand_draw_view)))
            ARHandsComponentHelper.startHand(
                mActivity, window!!.decorView, null, null)
        
        // 각각의 키보드 레이아웃을 해당되는 클래스에서 컨트롤 하도록 함.
        keypadEnglish = KeypadEnglish(context, findViewById(R.id.bottom_layout_english), this)
        keypadKorean = KeypadKorean(context, findViewById(R.id.bottom_layout_korea), this)
        keypadSpecial = KeypadSpecial(context, findViewById(R.id.bottom_layout_special), this)
        keypadNumber = KeypadNumber(context, findViewById(R.id.bottom_layout_number), this)

        // 이전의 선택된 키패드를 설정
        setKeypad(EditingText.currentKeypadType)

        // 각각의 키보드에서 입력한 문자는 EditingText에 기록되며, 기록된 내용이 변경되면 아래의 콜백으로 변경된 내용을 받는다.
        EditingText.setListenerOnChangeEditingText {
            // 변경된 내용을 입력창에 반영
            inputData?.setText(it)
        }

        // 입력창의 커서 위치가 변하면 알려주는 콜백
        EditingText.setListenerOnChangeCursorPosition {
            Log.d("keypad_dialog", "changed now cursor position : $it")
            val length = EditingText.editingText.length
            if (it > length) {
                inputData?.setSelection(length)
            } else {
                inputData?.setSelection(it)
            }
        }

        // 키보드 타입이 변경되면 현재 다이얼로그에서 키보드 타입을 변경한다. (EditText_에서 키보드 타입 변경시 콜백받음)
        EditingText.setListenerOnChangeKeypadType {
            setKeypad(it)
        }

        // 각각의 키패드 클래스(ex: com.example.mymediapipeex.KeypadEnglish, com.example.mymediapipeex.KeypadKorean..) 에서 KeypadDialog를 닫고 싶을때 호출
        EditingText.setDismissListener {
            dismiss()
        }


    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun setKeypad(type: TypeKeypad) {
        when (type) {
            TypeKeypad.English -> {
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT
                keypadEnglish?.enable()
                keypadEnglish?.disableEmailMode()
                keypadKorean?.disable()
                keypadSpecial?.disable()
                keypadNumber?.disable()
            }
            TypeKeypad.Korean -> {
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT
                keypadKorean?.enable()
                keypadSpecial?.disable()
                keypadEnglish?.disable()
                keypadNumber?.disable()
            }
            TypeKeypad.Special -> {
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT
                keypadSpecial?.enable()
                keypadEnglish?.disable()
                keypadKorean?.disable()
                keypadNumber?.disable()
            }
            TypeKeypad.Email -> {
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT
                keypadEnglish?.enable()
                keypadEnglish?.enableEmailMode()
                keypadKorean?.disable()
                keypadSpecial?.disable()
                keypadNumber?.disable()
            }
            TypeKeypad.Number -> {
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT
                keypadNumber?.enable()
                keypadEnglish?.disable()
                keypadKorean?.disable()
                keypadSpecial?.disable()
            }
            TypeKeypad.Password -> {
                keypadEnglish?.enable()
                keypadEnglish?.enablePasswordMode()
                keypadNumber?.disable()
                keypadKorean?.disable()
                keypadSpecial?.disable()
                //inputData?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
        }
    }

    fun showDialog(currentText: String?) {
        Log.d("keypad_dialog", "show key pad dialog")

        if (currentText == null) {
            EditingText.setRestoreText("")
            EditingText.editingText = ""
            EditingText.nowCursorPosition = 0
        } else {
            EditingText.setRestoreText(currentText)
            EditingText.editingText = currentText
            EditingText.nowCursorPosition = currentText.length
        }


        if (!isShowing)
            show()


    }


}