package com.example.mymediapipeex

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import java.util.*
import kotlin.collections.ArrayList

class KeypadKorean(private val context: Context, private val rootLayout:LinearLayout, val keypadDialog: KeypadDialog) {

    private var cho: Char = '\u0000'
    private var jun: Char = '\u0000'
    private var jon: Char = '\u0000'
    private var jonFlag:Char = '\u0000'
    private var doubleJonFlag:Char = '\u0000'
    var junFlag:Char = '\u0000'

    private val chos: List<Int> = listOf(0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141,0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e)
    private val juns:List<Int> = listOf(0x314f, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154, 0x3155, 0x3156, 0x3157, 0x3158, 0x3159, 0x315a, 0x315b, 0x315c, 0x315d, 0x315e, 0x315f, 0x3160, 0x3161, 0x3162, 0x3163)
    private val jons:List<Int> = listOf(0x0000, 0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136, 0x3137, 0x3139, 0x313a, 0x313b, 0x313c, 0x313d, 0x313e, 0x313f, 0x3140, 0x3141, 0x3142, 0x3144, 0x3145, 0x3146, 0x3147, 0x3148, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e)

    var state = 0
    var prevState = 0
    var prevText = ""

    private val letterLines = listOf(
        KeypadData.hFirstLine,
        KeypadData.hSecondLine,
        KeypadData.hThirdLine,
        KeypadData.optionLine
    )

    private var firstLineTextViews = ArrayList<TextView>()

    private var isShiftDown = false


    init {
        initUI()
        initKeyboard()
    }

    fun enable(){
        rootLayout.visibility = View.VISIBLE
    }

    fun disable(){
        rootLayout.visibility = View.GONE
    }


    private fun initUI(){
        rootLayout.run {
            findViewById<TextView>(R.id.tv_delete).setOnClickListener {
                delete()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_delete))

            findViewById<TextView>(R.id.tv_clear).setOnClickListener {
                initHangulMaker()
                EditingText.clearText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_clear))


            findViewById<TextView>(R.id.tv_move_first).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveFirst()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_first))

            findViewById<TextView>(R.id.tv_move_last).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveLast()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_last))

            findViewById<TextView>(R.id.tv_move_left).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveLeft(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left))

            findViewById<TextView>(R.id.tv_move_right).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveRight(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right))

            findViewById<TextView>(R.id.tv_move_left_word).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveLeftWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left_word))

            findViewById<TextView>(R.id.tv_move_right_word).setOnClickListener {
                initHangulMaker()
                EditingText.cursor.moveRightWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right_word))

        }
    }

    private inline fun <reified T>getChildArray(targetView: ViewGroup, type:T): java.util.ArrayList<T> {
        val array = java.util.ArrayList<T>()

        val countChild = targetView.childCount
        for(i in 0 until countChild){
            if(targetView.getChildAt(i) is T){
                array.add(targetView.getChildAt(i) as T)
            }
        }
        return array
    }

    @SuppressLint("SetTextI18n")
    private fun initKeyboard(){
        val lines = getChildArray(rootLayout.findViewById<LinearLayout>(R.id.bottom_layout_korea), LinearLayout(context))

        // number line (숫자열)
        val countChild = lines[0].childCount
        for(i in 0 until countChild){

            if(lines[0].getChildAt(i) is LinearLayout){
                val ll = lines[0].getChildAt(i) as LinearLayout
                val childTextViews = getChildArray(ll, TextView(context))
                childTextViews[1].text = KeypadData.numberLine[i]
                if(Locale.getDefault() == Locale.KOREA){
                    childTextViews[0].visibility = View.VISIBLE
                    childTextViews[0].text = "숫자"
                    childTextViews[2].setOnClickListener {
                        inputOtherText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(childTextViews[2])

                }else{
                    childTextViews[0].visibility = View.GONE
                    childTextViews[1].setOnClickListener {
                        inputOtherText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(childTextViews[1])

                }
            }else{
                if(lines[0].getChildAt(i) is TextView){
                    val textView = lines[0].getChildAt(i) as TextView
                    textView.text = KeypadData.numberLine[i]
                    textView.setOnClickListener{
                        inputOtherText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(textView)

                }
            }
        }




        // 쉬프트 눌렀을때의 변경문자 (쌍자음, 이중모음)
        val firstLineConstraint = getChildArray(lines[1], ConstraintLayout(context))
        for(i in firstLineConstraint.indices){
            firstLineTextViews.add(firstLineConstraint[i].getChildAt(1) as TextView)
        }




        for(i in 1..3){
            // 한글배치
            val lineElements = getChildArray(lines[i], ConstraintLayout(context))
            for(j in lineElements.indices){
                val englishHelper = lineElements[j].getChildAt(0) as TextView
                englishHelper.text = "${i*10+j}"
                val letterText = lineElements[j].getChildAt(1) as TextView
                letterText.text = letterLines[i-1][j]

                letterText.setOnClickListener {
                    val char = letterText.text.toString().toCharArray().first()
                    commit(char)
                    if(isShiftDown){
                        changeShiftLetter()
                    }
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(letterText)

                englishHelper.setOnClickListener {
                    val char = letterText.text.toString().toCharArray().first()
                    commit(char)
                    if(isShiftDown){
                        changeShiftLetter()
                    }
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(englishHelper)

            }

            // 옵션키 - 쉬프트, 영문전환
            if(i == 3){
                val lineElementsOption = getChildArray(lines[i], TextView(context))
                lineElementsOption[0].text = context.resources.getString(R.string.custom_keypad_shift)
                lineElementsOption[0].setOnClickListener {
                    changeShiftLetter()
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOption[0])

                lineElementsOption[1].text = context.resources.getString(R.string.custom_keypad_english_txt)
                lineElementsOption[1].setOnClickListener {
                    EditingText.currentKeypadType = TypeKeypad.English
                    //Navigation.findNavController(requireView()).navigate(R.id.action_keypadKoreaFragment_to_keypadEnglishFragment)
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOption[1])
            }


        }

        // 기기의 언어가 한글과 영어일때 표시되는 옵션 글자 다르게 설정
        val lineElementsOption = getChildArray(lines[4], TextView(context))
        for(j in lineElementsOption.indices){
            if(Locale.getDefault() == Locale.KOREA){
                lineElementsOption[j].text = KeypadData.optionLine[j]
            }else{
                lineElementsOption[j].text = KeypadData.optionLineEn[j]
            }
            setOptionKeyClickListener(j, lineElementsOption[j])
        }


        // 추가 음성 명령 - 인식이 잘 안되는 문자들 추가로 입력
        addCommand("히응"){
            val text = "ㅎ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("티읏"){
            val text = "ㅌ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("치읏"){
            val text = "ㅊ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("비읍"){
            val text = "ㅂ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("피읍"){
            val text = "ㅍ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("시옷"){
            val text = "ㅅ"
            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }

        addCommand("십팔"){
            var text = "ㅐ"
            if(isShiftDown){
                text = "ㅒ"
            }

            val char = text.toCharArray().first()
            commit(char)
            if(isShiftDown){
                changeShiftLetter()
            }
        }


        addCommand("영문전환"){
            EditingText.currentKeypadType = TypeKeypad.English
            //Navigation.findNavController(requireView()).navigate(R.id.action_keypadKoreaFragment_to_keypadEnglishFragment)
        }


        addCommand("시프트"){
            changeShiftLetter()
        }

        addCommand("영"){
            inputOtherText("0")
        }

    }


    private fun setOptionKeyClickListener(index:Int, textView: TextView){
        textView.setOnClickListener {
            when(index){
                0->{ // special key
                    EditingText.currentKeypadType = TypeKeypad.Special
                    //Navigation.findNavController(requireView()).navigate(R.id.action_keypadKoreaFragment_to_keypadSpecialFragment)
                }
                1->{ // .com key
                    initHangulMaker()
                    inputOtherText(".com")
                }
                2->{ // period key
                    initHangulMaker()
                    inputOtherText(".")
                }
                3->{ // space key
                    initHangulMaker()
                    inputOtherText(" ")
                }
                4->{ // cancel key
                    EditingText.restoreText()
                    initHangulMaker()
                    EditingText.dismissDialog()
                }
                5->{ // ok key
                    initHangulMaker()
                    EditingText.dismissDialog()
                }
            }
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
    }






    private fun addCommand(commandText:String, onClickCallback:()->Unit){
        val textView = TextView(context)
        textView.text = commandText
        textView.contentDescription = "hf_no_number"
        val lp = LinearLayout.LayoutParams(1,1)
        textView.layoutParams = lp
        textView.setOnClickListener {
            onClickCallback()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
        rootLayout.findViewById<LinearLayout>(R.id.ll_command).addView(textView)
    }


    // 쉬프트를 눌렀을때 다르게 보일 문자 표시
    private fun changeShiftLetter(){
        Log.d("korean_keypad", "firstlinetextview size : ${firstLineTextViews.size} , isShiftDown == $isShiftDown")
        for(i in firstLineTextViews.indices){
            firstLineTextViews[i].text = if(isShiftDown){
                KeypadData.hFirstLine[i]
            }else{
                KeypadData.hShiftFirstLine[i]
            }
        }
        isShiftDown = !isShiftDown
    }



    //한글 (초성, 중성, 종성 조합) 로직 초기화
    private fun initHangulMaker(){
        state = 0
        prevState = 0
        prevText =""
        clear()
    }

    //마지막 입력문자 지움
    private fun deleteLastString(){
        subText()
    }


    //한글 (초성, 중성, 종성) 조합 로직
    private fun makeHan():Char{
        if(state == 0){
            return '\u0000'
        }
        if(state == 1){
            return cho
        }
        val choIndex = chos.indexOf(cho.toInt())
        val junIndex = juns.indexOf(jun.toInt())
        val jonIndex = jons.indexOf(jon.toInt())

        val makeResult = 0xAC00 + 28 * 21 * (choIndex) + 28 * (junIndex)  + jonIndex

        return makeResult.toChar()
    }

    private fun clear(){
        cho = '\u0000'
        jun = '\u0000'
        jon = '\u0000'
        jonFlag = '\u0000'
        doubleJonFlag = '\u0000'
        junFlag = '\u0000'
    }


    private fun addTexts(str:String){
        Log.d("korean_keypad", "addTexts -> str length : ${str.length}")
        val currentText = EditingText.editingText
        if(currentText.isEmpty()){
            Log.e("korean_keypad","current text is empty")
            EditingText.editingText = str
        } else{
            val nowPosition = EditingText.nowCursorPosition

            val begin = currentText.substring(0, nowPosition)
            val end = currentText.substring(nowPosition)

            var completeText = begin
            completeText += str
            completeText += end
            EditingText.editingText = completeText
            Log.d("korean_keypad", "addText after text : $completeText")
        }
    }

    private fun subText(){
        val currentText = EditingText.editingText
        if(currentText.isEmpty())
            return

        var nowPosition = EditingText.nowCursorPosition
        Log.d("korean_keypad", "before subtext ----- currentText : $currentText, nowPosition : $nowPosition, textLength : ${currentText.length}")

        if(nowPosition > currentText.length){
            Log.d("korean_keypad", "nowPosition changed --- $nowPosition -> ${currentText.length}")
            nowPosition = currentText.length
        }

        if(nowPosition > 0){
            val completeText = currentText.removeRange(nowPosition-1, nowPosition)
            EditingText.editingText = completeText
            EditingText.nowCursorPosition = nowPosition-1
        }

        Log.d("korean_keypad", "after subtext ----- currentText : ${EditingText.editingText}, nowPosition : ${EditingText.nowCursorPosition}, textLength : ${EditingText.editingText.length}")
    }

    // 한글이 아닌 글자를 추가할때
    private fun inputOtherText(str:String){
        initHangulMaker()
        addTexts(str)
        EditingText.cursor.moveRight(str.length)
    }

    private fun inputText(str:String){

        if(prevState ==1 && state ==1){
            Log.d("korean_keypad", "prevState ==1 && state ==1")

        }else if(state == 0){
            Log.d("korean_keypad", "state == 0")
        }else if(prevState == 3 && state == 1){
            Log.d("korean_keypad", "prevState == 3 && state == 1")
        }else if(prevState == 3 && state == 2){
            if(isDeleting){
                deleteLastString()
            }
        }
        else{
            if(prevState != 0 && prevState != 3){
                deleteLastString()
            }

            if(prevState == 3 && state == 3){
                deleteLastString()
            }
        }


        when(state){
            0->{
                addTexts(str)
            }
            1->{
                addTexts(str)
            }
            2->{
                if(prevState == 3){
                    addTexts(str)
                }
                else{
                    addTexts(str)
                }
            }
            3->{
                addTexts(str)
            }

        }

        prevState = state
        prevText = str

        EditingText.cursor.moveRight(1)
        isDeleting = false

    }


    private fun commit(c:Char){

        // 초성 중성 종성에 해당 글자가 없는 경우
        if(chos.indexOf(c.toInt()) < 0 && juns.indexOf(c.toInt()) < 0 && jons.indexOf(c.toInt()) < 0){
            directlyCommit()
            inputText(c.toString())
            return
        }
        when(state){
            0 -> {
                if(juns.indexOf(c.toInt()) >= 0){
                    inputText(c.toString())
                    clear()
                }else{//초성일 경우
                    state = 1
                    cho = c
                    inputText(cho.toString())
                }
            }
            1 -> {
                if(chos.indexOf(c.toInt()) >= 0){
                    cho = c
                    inputText(cho.toString())
                    //clear()
                }else{//중성일 경우
                    state = 2
                    jun = c
                    inputText(makeHan().toString())
                }
            }
            2 -> {
                if(juns.indexOf(c.toInt()) >= 0){
                    if(doubleJunEnable(c)){
                        inputText(makeHan().toString())
                    }
                    else{
                        inputText(makeHan().toString())
                        state = 0
                        inputText(c.toString())
                        clear()
                    }
                }
                else if(jons.indexOf(c.toInt()) >= 0){//종성이 들어왔을 경우
                    jon = c
                    state = 3
                    inputText(makeHan().toString())
                }
                else{
                    directlyCommit()
                    cho = c
                    state = 1
                    inputText(makeHan().toString())
                }
            }
            3 -> {
                if(jons.indexOf(c.toInt()) >= 0){
                    if(doubleJonEnable(c)){
                        inputText(makeHan().toString())
                    }
                    else{
                        inputText(makeHan().toString())
                        clear()
                        state = 1
                        cho = c
                        inputText(cho.toString())

                    }

                }
                else if(chos.indexOf(c.toInt()) >= 0){
                    inputText(makeHan().toString())
                    state = 1
                    clear()
                    cho = c
                    inputText(cho.toString())
                }
                else{//중성이 들어올 경우
                    var temp:Char = '\u0000'
                    if(doubleJonFlag == '\u0000'){
                        temp = jon
                        jon = '\u0000'
                        inputText(makeHan().toString())
                    }
                    else{
                        temp = doubleJonFlag
                        jon = jonFlag
                        inputText(makeHan().toString())
                    }
                    state = 2
                    clear()
                    cho = temp
                    jun = c
                    inputText(makeHan().toString())
                }
            }
        }
    }



    private fun directlyCommit(){
        if(state == 0){
            return
        }
        inputText(makeHan().toString())
        state = 0
        clear()
    }

    private var isDeleting = false

    private fun delete(){
        isDeleting = true
        Log.d("korean_keypad", "delete() state : $state")
        when(state){
            0 -> {
                Log.d("korean_keypad", "delete 0")
                deleteLastString()
            }
            1 -> {
                Log.d("korean_keypad", "delete 1")
                cho = '\u0000'
                state = 0

                deleteLastString()
            }
            2 -> {
                if(junFlag != '\u0000'){
                    Log.d("korean_keypad", "jun flag != u0000")
                    jun = junFlag
                    junFlag = '\u0000'
                    state = 2
                    inputText(makeHan().toString())
                }
                else{
                    Log.d("korean_keypad", "jun flag == u0000")
                    jun = '\u0000'
                    junFlag = '\u0000'
                    state = 1
                    inputText(cho.toString())
                }
            }
            3 -> {
                if(doubleJonFlag == '\u0000'){
                    jon = '\u0000'
                    state = 2
                }
                else{
                    jon = jonFlag
                    jonFlag = '\u0000'
                    doubleJonFlag = '\u0000'
                    state = 3
                }
                inputText(makeHan().toString())
            }
        }
    }

    private fun doubleJunEnable(c:Char):Boolean{
        when(jun){
            'ㅗ' -> {
                if(c == 'ㅏ'){
                    junFlag = jun
                    jun = 'ㅘ'
                    return true
                }
                if(c == 'ㅐ'){
                    junFlag = jun
                    jun = 'ㅙ'
                    return true
                }
                if(c == 'ㅣ'){
                    junFlag = jun
                    jun = 'ㅚ'
                    return true
                }
                return false
            }
            'ㅜ' -> {
                if(c == 'ㅓ'){
                    junFlag = jun
                    jun = 'ㅝ'
                    return true
                }
                if(c == 'ㅔ'){
                    junFlag = jun
                    jun = 'ㅞ'
                    return true
                }
                if(c == 'ㅣ'){
                    junFlag = jun
                    jun = 'ㅟ'
                    return true
                }
                return false
            }
            'ㅡ' -> {
                if(c == 'ㅣ'){
                    junFlag = jun
                    jun = 'ㅢ'
                    return true
                }
                return false
            }
            else -> {
                return false
            }
        }
    }

    private fun doubleJonEnable(c:Char):Boolean{
        jonFlag = jon
        doubleJonFlag = c
        when(jon){
            'ㄱ' -> {
                if(c == 'ㅅ'){
                    jon = 'ㄳ'
                    return true
                }
                return false
            }
            'ㄴ' -> {
                if(c == 'ㅈ'){
                    jon = 'ㄵ'
                    return true
                }
                if(c == 'ㅎ'){
                    jon = 'ㄶ'
                    return true
                }
                return false
            }
            'ㄹ' -> {
                if(c == 'ㄱ'){
                    jon = 'ㄺ'
                    return true
                }
                if(c == 'ㅁ'){
                    jon = 'ㄻ'
                    return true
                }
                if(c == 'ㅂ'){
                    jon = 'ㄼ'
                    return true
                }
                if(c == 'ㅅ'){
                    jon = 'ㄽ'
                    return true
                }
                if(c == 'ㅌ'){
                    jon = 'ㄾ'
                    return true
                }
                if(c == 'ㅍ'){
                    jon = 'ㄿ'
                    return true
                }
                if(c == 'ㅎ'){
                    jon = 'ㅀ'
                    return true
                }
                return false
            }
            'ㅂ' -> {
                if(c == 'ㅅ'){
                    jon = 'ㅄ'
                    return true
                }
                return false
            }
            else -> {
                return false
            }
        }
    }
    private fun junAvailable():Boolean{
        if(jun == 'ㅙ' || jun == 'ㅞ' || jun == 'ㅢ'|| jun == 'ㅐ' || jun == 'ㅔ' || jun == 'ㅛ' || jun == 'ㅒ' || jun == 'ㅖ'){
            return false
        }
        return true
    }

    private fun isDoubleJun():Boolean{
        if(jun == 'ㅙ' || jun == 'ㅞ' || jun == 'ㅚ'|| jun == 'ㅝ' || jun == 'ㅟ' || jun == 'ㅘ' || jun == 'ㅢ'){
            return true
        }
        return false
    }

}