package com.example.mymediapipeex

enum class TypeKeypad {
    English,
    Korean,
    Special,
    Email,
    Number,
    Password
}