package com.example.mymediapipeex

import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.InputType
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.appcompat.widget.AppCompatEditText


class EditText_: AppCompatEditText, View.OnClickListener {
    //호출 하는 Activity
    var mActivity: Activity? = null

    private val TAG = "WattKeyboard"
    
    private val realwearModelNames = listOf("T1100G", "T1100S", "T1200G", "T21G")

    private var keypadDialog: KeypadDialog? = null

    private var mContext:Context?=null

    private var keypadType: TypeKeypad = TypeKeypad.English

    // EditText를 상속하기 때문에 생성자 정의 해야 함.
   constructor(context: Context) : super(context) {
        // 목록에 포함된 리얼웨어 모델 기종에 포함되어 있는지 확인 ("T1100G", "T1100S", "T1200G", "T21G")
       if(isRealwearModelName())
           initView(context)
       else
           Log.e("WattKeyboard","This model is not realwear")
   }

    // EditText를 상속하기 때문에 생성자 정의 해야 함.
    constructor(context: Context, attrs: AttributeSet):super(context, attrs){
        if(isRealwearModelName())
            initView(context)
        else
            Log.e("WattKeyboard","This model is not realwear")

    }

    // EditText를 상속하기 때문에 생성자 정의 해야 함.
    constructor(context: Context, attrs: AttributeSet, defSty: Int):super(context, attrs, defSty){
        if(isRealwearModelName())
            initView(context)
        else
            Log.e("WattKeyboard","This model is not realwear")

    }

    private fun initView(context: Context){
        mContext = context

        keypadDialog = mActivity?.let { KeypadDialog(context, it) }

        keypadDialog?.setCancelable(true)
        keypadDialog?.window?.setGravity(Gravity.BOTTOM)
        //다이얼로그가 닫힐때 다이얼로그에서 입력한 텍스트값(EditingText.editingText)을 현재의 클래스(com.example.mymediapipeex.EditText_)에 입력한다.
        keypadDialog?.setOnCancelListener {
            Log.d(TAG, "on cancel listener")
            setText(EditingText.editingText)
            callbackOnClose?.let{ onClose->
                onClose()
            }
        }

        //다이얼로그가 닫힐때 다이얼로그에서 입력한 텍스트값(EditingText.editingText)을 현재의 클래스(com.example.mymediapipeex.EditText_)에 입력한다.
        keypadDialog?.setOnDismissListener {
            Log.d(TAG, "on dismiss listener")
            setText(EditingText.editingText)
            callbackOnClose?.let{ onClose->
                onClose()
            }
        }

        // 기본 키보드 나오는것 방지
        keypadDialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        this.isFocusableInTouchMode = false //10.5.0 버전 대응: v1.0.8 에서 추가됨


        super.setOnClickListener(this)
    }


    private var callbackOnClose:(()->Unit)?=null
    fun setOnCloseListener(callback:()->Unit){
        callbackOnClose = callback
    }

    // 키보드 타입을 설정
    fun setKeypadType(type: TypeKeypad){
        keypadType = type
        EditingText.currentKeypadType = type
        if(type == TypeKeypad.Password){
            this.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
    }

    // 설정한 리얼웨어 글라스 모델인지 판별
    private fun isRealwearModelName():Boolean{
        return realwearModelNames.contains(Build.MODEL)
    }

    // 키패드 다이얼로그를 보여준다.
    fun start(){
        if(isRealwearModelName()){
            mContext?.let{
                setKeypadType(keypadType)
                initView(it)
            }
            keypadDialog?.showDialog(text.toString())
        }
    }

    // 키패드 다이얼로그를 보여준다.
    override fun onClick(p0: View?) {
        if(isRealwearModelName()){
            mContext?.let{
                setKeypadType(keypadType)
                initView(it)
            }
            keypadDialog?.showDialog(text.toString())
        }
    }
}