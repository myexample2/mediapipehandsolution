package com.example.mymediapipeex

object KeypadData {
    val optionLine = listOf("특수문자","닷컴","마침표","스페이스","취소","확인")
    val optionLineEn = listOf("Special", ".com",".","Space","Cancel","Ok")

    val optionLineEmail = listOf("닷컴", "씨오점", "앳", "점")
    val optionLineEmailEn = listOf(".com",".co.kr","@",".")

    val numberLine = listOf("1","2","3","4","5","6","7","8","9","0")

    val hFirstLine = listOf("ㅂ","ㅈ","ㄷ","ㄱ","ㅅ","ㅛ","ㅕ","ㅑ","ㅐ","ㅔ")
    val hSecondLine = listOf("ㅁ","ㄴ","ㅇ","ㄹ","ㅎ","ㅗ","ㅓ","ㅏ","ㅣ")
    val hThirdLine = listOf("ㅋ","ㅌ","ㅊ","ㅍ","ㅠ","ㅜ","ㅡ")
    val hShiftFirstLine = listOf("ㅃ","ㅉ","ㄸ","ㄲ","ㅆ","ㅛ","ㅕ","ㅑ","ㅒ","ㅖ")

    val eBigFirstLine = listOf("Q","W","E","R","T","Y","U","I","O","P")
    val eBigSecondLine = listOf("A", "S", "D", "F", "G", "H", "J", "K", "L")
    val eBigThirdLine = listOf("Z", "X", "C", "V", "B", "N", "M")


    val eSmallFirstLine = listOf("q", "w", "e", "r", "t", "y", "u", "i", "o", "p")
    val eSmallSecondLine = listOf("a", "s", "d", "f", "g", "h", "j", "k", "l")
    val eSmallThirdLine = listOf("z", "x", "c", "v", "b", "n", "m")

    val sFirstLine = listOf("!", "@", "#", "$", "%", "^", "&", "*", "(", ")")
    val sSecondLine = listOf("-", "=", "+", "{", "}", "[", "]", "\\", ":", ";")
    val sThirdLine = listOf("\"", "'", "<", ">", ",", ".", "/", "?", "|")
    val sFourthLine = listOf("~", "`", "_", "※", "●", "■", "￦")


}