package com.example.mymediapipeex

import com.example.mymediapipeex.TypeKeypad
import android.annotation.SuppressLint
import android.util.Log
import kotlin.properties.Delegates


@SuppressLint("StaticFieldLeak")
object EditingText {

    // EidtText_에 입력될 문자 (KeypadDialog에서 입력된 문자를 KeypadDialog가 닫히면 EditText_에 입력해 준다)
    var editingText by Delegates.observable(""){ _,_,newValue->
        // eidtingText의 값이 변하면 변한 값을 callbackOnChangeEditingText에 전달
        callbackOnChangeEditingText?.let { it(newValue) }
    }

    private var callbackOnChangeEditingText:((String)->Unit)?=null

    fun setListenerOnChangeEditingText(callback:((String)->Unit)?){
        callbackOnChangeEditingText = callback
    }


    private var restoreText:String = ""


    private var callbackOnChangeKeypadType:((TypeKeypad)->Unit)?=null
    // english 0 , korean 1, special 2
    var currentKeypadType: TypeKeypad by Delegates.observable(TypeKeypad.English){ _, _, newValue->
        callbackOnChangeKeypadType?.let{it(newValue)}
    }

    fun setListenerOnChangeKeypadType(callback:((TypeKeypad)->Unit)?){
        callbackOnChangeKeypadType = callback
    }




    private var callbackOnChangeCursorPosition:((Int)->Unit)?=null

    fun setListenerOnChangeCursorPosition(callback:((Int)->Unit)?){
        callbackOnChangeCursorPosition = callback
    }


    // EditText에서 커서의 현재 위치
    var nowCursorPosition by Delegates.observable(0){ _,_,newValue->
        callbackOnChangeCursorPosition?.let{it(newValue)}
    }


    private var callbackDismissDialog:(()->Unit)?=null

    fun setRestoreText(txt:String){
        restoreText = txt
    }

    fun restoreText(){
        editingText = restoreText
    }

    fun setDismissListener(callback:()->Unit){
        Log.d("com.example.mymediapipeex.EditText_", "regist dismiss listener")
        callbackDismissDialog = callback
    }

    fun dismissDialog(){
        callbackDismissDialog?.let{
            Log.d("com.example.mymediapipeex.EditText_", "dismiss dialog on EditingText")
            it()
        }
    }

    fun clearText(){
        editingText = ""
    }

    val cursor = Cursor()


    private fun prevSpaceIndexOf(cursorIndex:Int):Int{
        var indexSpace = 0
        val startIndex = cursorIndex -1
        if(startIndex >= 0 && startIndex <= editingText.length){
            for(i in startIndex downTo 0){
                Log.d("com.example.mymediapipeex.EditText_", "edittext[$i] : ${editingText[i]}")
                if(editingText[i] == ' '){
                    Log.d("com.example.mymediapipeex.EditText_", "editText[i] == ' '")
                    val prevIndex = i-1
                    if(prevIndex >=0 && editingText[prevIndex] != ' '){
                        indexSpace = i
                        break
                    }
                }
            }
        }
        return indexSpace
    }


    private fun nextSpaceIndexOf(cursorIndex:Int):Int{

        var indexSpace = editingText.length

        //val startIndex = cursorIndex + 1
        Log.d("com.example.mymediapipeex.EditText_", "cursorIndex : $cursorIndex, length : ${editingText.length}")
        if(cursorIndex >= editingText.length){
            return cursorIndex
        }

        if(cursorIndex >= 0 && cursorIndex < editingText.length){
            for(i in cursorIndex until editingText.length){
                Log.d("com.example.mymediapipeex.EditText_", "edittext[$i] : ${editingText[i]}")
                if(editingText[i] == ' '){
                    if(i==cursorIndex)
                        continue
                    Log.d("com.example.mymediapipeex.EditText_", "editText[i] == ' '")
                    val prevIndex = i-1
                    if(prevIndex >=0 && editingText[prevIndex] != ' '){
                        indexSpace = i
                        break
                    }
                }
            }
        }
        return indexSpace
    }




    class Cursor{
        fun moveLeftWord(){
            nowCursorPosition.let {
                val index = prevSpaceIndexOf(it)
                Log.d("com.example.mymediapipeex.EditText_", "prev space index of : $index")
                nowCursorPosition = index
            }
        }

        fun moveRightWord(){
            nowCursorPosition.let {
                val index = nextSpaceIndexOf(it)
                Log.d("com.example.mymediapipeex.EditText_", "next space index of : $index")
                nowCursorPosition = index
            }
        }

        fun moveRight(add:Int){
            nowCursorPosition.let{
                val length = editingText.length ?: 0
                if(it+add <= length){
                    nowCursorPosition = it + add
                }else{
                    nowCursorPosition = length
                }
            }
        }

        fun moveLeft(sub:Int){
            nowCursorPosition.let{
                val completePosition = it - sub
                if(completePosition >= 0){
                    nowCursorPosition = completePosition
                }

            }
        }

        fun moveFirst(){
            nowCursorPosition = 0
        }

        fun moveLast(){
            val length = editingText.length ?: 0
            nowCursorPosition = length
        }
    }



}