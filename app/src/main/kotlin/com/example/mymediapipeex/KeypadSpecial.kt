package com.example.mymediapipeex

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import java.util.*

class KeypadSpecial(private val context:Context, private val rootLayout: LinearLayout, val keypadDialog: KeypadDialog) {
    private val letterLinesSpecial = listOf(
        KeypadData.sFirstLine,
        KeypadData.sSecondLine,
        KeypadData.sThirdLine,
        KeypadData.sFourthLine
    )

    init {
        initUI()
        initKeyboard()
    }

    fun enable(){
        rootLayout.visibility = View.VISIBLE
    }

    fun disable(){
        rootLayout.visibility = View.GONE
    }

    private fun initUI(){
        rootLayout.run {
            findViewById<TextView>(R.id.tv_delete).setOnClickListener {
                subText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_delete))

            findViewById<TextView>(R.id.tv_clear).setOnClickListener {
                EditingText.clearText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_clear))

            findViewById<TextView>(R.id.tv_move_first).setOnClickListener {
                EditingText.cursor.moveFirst()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_first))

            findViewById<TextView>(R.id.tv_move_last).setOnClickListener {
                EditingText.cursor.moveLast()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_last))

            findViewById<TextView>(R.id.tv_move_left).setOnClickListener {
                EditingText.cursor.moveLeft(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left))

            findViewById<TextView>(R.id.tv_move_right).setOnClickListener {
                EditingText.cursor.moveRight(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right))

            findViewById<TextView>(R.id.tv_move_left_word).setOnClickListener {
                EditingText.cursor.moveLeftWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left_word))

            findViewById<TextView>(R.id.tv_move_right_word).setOnClickListener {
                EditingText.cursor.moveRightWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right_word))

        }
    }


    private inline fun <reified T>getChildArray(targetView: ViewGroup, type:T): java.util.ArrayList<T> {
        val array = java.util.ArrayList<T>()

        val countChild = targetView.childCount
        for(i in 0 until countChild){
            if(targetView.getChildAt(i) is T){
                array.add(targetView.getChildAt(i) as T)
            }
        }
        return array
    }


    @SuppressLint("SetTextI18n")
    private fun initKeyboard(){
        val lines = getChildArray(rootLayout.findViewById<LinearLayout>(R.id.bottom_layout_special), LinearLayout(context))


        for(i in 0..3){
            val lineElements = getChildArray(lines[i], ConstraintLayout(context))

            for(j in lineElements.indices){
                val englishHelper = lineElements[j].getChildAt(0) as TextView
                englishHelper.text = "${i*10+j+1}"
                val letterText = lineElements[j].getChildAt(1) as TextView
                letterText.text = letterLinesSpecial[i][j]

                englishHelper.setOnClickListener {
                    inputText(letterText.text.toString())
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(englishHelper)
            }
        }

        val lineElementsOptionLine3 = getChildArray(lines[3], TextView(context))
        lineElementsOptionLine3[0].text = context.resources.getString(R.string.custom_keypad_english_txt)
        lineElementsOptionLine3[0].setOnClickListener {
            EditingText.currentKeypadType = TypeKeypad.English
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOptionLine3[0])

        if(Locale.getDefault() == Locale.KOREA){
            lineElementsOptionLine3[1].visibility = View.VISIBLE
            lineElementsOptionLine3[1].text = context.resources.getString(R.string.custom_keypad_korea_txt)
            lineElementsOptionLine3[1].setOnClickListener {
                EditingText.currentKeypadType = TypeKeypad.Korean
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOptionLine3[1])
        }else{
            lineElementsOptionLine3[1].visibility = View.GONE
        }




        val lineElementsOption = getChildArray(lines[4], TextView(context))
        for(j in lineElementsOption.indices){
            if(Locale.getDefault() == Locale.KOREA){
                lineElementsOption[j].text = KeypadData.optionLine[j]
            }else{
                lineElementsOption[j].text = KeypadData.optionLineEn[j]
            }
            setOptionKeyClickListener(j, lineElementsOption[j])
        }


        //추가 음성 입력
        if(Locale.getDefault() == Locale.KOREA){
            addCommand("십팔"){
                inputText(letterLinesSpecial[1][7])
            }
        }

        addCommand("dot"){
            inputText(".")
        }

    }


    private fun setOptionKeyClickListener(index:Int, textView: TextView){
        textView.setOnClickListener {
            when(index){
                0->{ // special key

                }
                1->{ // .com key
                    inputText(".com")
                }
                2->{ // period key
                    inputText(".")
                }
                3->{ // space key
                    inputText(" ")
                }
                4->{ // cancel key
                    EditingText.restoreText()
                    EditingText.dismissDialog()
                }
                5->{ // ok key
                    EditingText.dismissDialog()
                }
            }
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
    }


    private fun addCommand(commandText:String, onClickCallback:()->Unit){
        val textView = TextView(context)
        textView.text = commandText
        textView.contentDescription = "hf_no_number"
        val lp = LinearLayout.LayoutParams(1,1)
        textView.layoutParams = lp
        textView.setOnClickListener {
            onClickCallback()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
        rootLayout.findViewById<LinearLayout>(R.id.ll_command).addView(textView)
    }



    private fun inputText(str:String){
        addTexts(str)
        EditingText.cursor.moveRight(str.length)
    }


    private fun addTexts(str:String){
        Log.d("special_keypad", "addTexts -> str length : ${str.length}")
        val currentText = EditingText.editingText
        if(currentText.isEmpty()){
            Log.e("special_keypad","current text is empty")
            EditingText.editingText = str
        } else{
            val nowPosition = EditingText.nowCursorPosition

            val begin = currentText.substring(0, nowPosition)
            val end = currentText.substring(nowPosition)

            var completeText = begin
            completeText += str
            completeText += end
            EditingText.editingText = completeText
            Log.d("special_keypad", "addText after text : $completeText")
        }
    }

    private fun subText(){
        val currentText = EditingText.editingText
        if(currentText.isEmpty())
            return

        var nowPosition = EditingText.nowCursorPosition
        Log.d("special_keypad", "before subtext ----- currentText : $currentText, nowPosition : $nowPosition, textLength : ${currentText.length}")

        if(nowPosition > currentText.length){
            Log.d("special_keypad", "nowPosition changed --- $nowPosition -> ${currentText.length}")
            nowPosition = currentText.length
        }

        if(nowPosition > 0){
            val completeText = currentText.removeRange(nowPosition-1, nowPosition)
            EditingText.editingText = completeText
            EditingText.nowCursorPosition = nowPosition-1
        }

        Log.d("special_keypad", "after subtext ----- currentText : ${EditingText.editingText}, nowPosition : ${EditingText.nowCursorPosition}, textLength : ${EditingText.editingText.length}")
    }





}