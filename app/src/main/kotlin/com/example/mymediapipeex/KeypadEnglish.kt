package com.example.mymediapipeex

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.mymediapipeex.utils.MDEBUG
import java.util.*
import kotlin.collections.ArrayList
import kotlin.properties.Delegates

class KeypadEnglish(private val context: Context, private val rootLayout: LinearLayout, val keypadDialog: KeypadDialog) {

    private val letterSmallLines = listOf(
        KeypadData.eSmallFirstLine,
        KeypadData.eSmallSecondLine,
        KeypadData.eSmallThirdLine
    )
    private val letterBigLines = listOf(
        KeypadData.eBigFirstLine,
        KeypadData.eBigSecondLine,
        KeypadData.eBigThirdLine
    )

    private val originHelperNumbers = listOf("10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","30","31","32","33","34","35","36")
    private var shuffleHelperNumbers = arrayListOf("10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","30","31","32","33","34","35","36")

    // 대문자인지 구분
    private var isUpper by Delegates.observable(false) { _, _, newValue ->
        if(newValue){
            changeUpperKeyboard()
        }else{
            changeLowerKeyboard()
        }
    }

    private var letterSmallTextViewsList = ArrayList<ArrayList<TextView>>()
    private var letterBigTextViewsList = ArrayList<ArrayList<TextView>>()

    private var llOptionKeys:LinearLayout?=null
    private var llOptionKeysEmail:LinearLayout?=null

    // 대문자로 전환
    private fun changeUpperKeyboard(){
        for(i in letterSmallTextViewsList.indices){
            for(j in letterSmallTextViewsList[i].indices){
                letterSmallTextViewsList[i][j].visibility = View.GONE
                letterBigTextViewsList[i][j].visibility = View.VISIBLE
            }
        }
    }

    // 소문자로 전환환
   private fun changeLowerKeyboard(){
        for(i in letterSmallTextViewsList.indices){
            for(j in letterSmallTextViewsList[i].indices){
                letterSmallTextViewsList[i][j].visibility = View.VISIBLE
                letterBigTextViewsList[i][j].visibility = View.GONE
            }
        }
    }



    init {
        Log.i("com.example.mymediapipeex.KeypadEnglish","init ui")
        initUI()
        initKeyboard()
    }


    // 키보드 활성화
    fun enable(){
        rootLayout.visibility = View.VISIBLE
    }

    // 키보드 비활성화
    fun disable(){
        rootLayout.visibility = View.GONE
    }

    // Email모드 (최하단 키패드가 변경됨 @를 입력할 수 있게)
    fun enableEmailMode(){
        llOptionKeys?.visibility = View.GONE
        llOptionKeysEmail?.visibility = View.VISIBLE
    }

    fun disableEmailMode(){
        llOptionKeys?.visibility = View.VISIBLE
        llOptionKeysEmail?.visibility = View.GONE
    }

    // 패스워드 모드일때 글자 옆의 숫자헬퍼가 랜덤으로 배치됨
    fun enablePasswordMode(){
        disableEmailMode()
    }




    private fun initUI(){
        rootLayout.run {
            findViewById<TextView>(R.id.tv_delete).setOnClickListener {
                subText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_delete))

            findViewById<TextView>(R.id.tv_clear).setOnClickListener {
                EditingText.clearText()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_clear))

            findViewById<TextView>(R.id.tv_move_first).setOnClickListener {
                EditingText.cursor.moveFirst()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_first))

            findViewById<TextView>(R.id.tv_move_last).setOnClickListener {
                EditingText.cursor.moveLast()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_last))

            findViewById<TextView>(R.id.tv_move_left).setOnClickListener {
                EditingText.cursor.moveLeft(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left))

            findViewById<TextView>(R.id.tv_move_right).setOnClickListener {
                EditingText.cursor.moveRight(1)
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right))

            findViewById<TextView>(R.id.tv_move_left_word).setOnClickListener {
                EditingText.cursor.moveLeftWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_left_word))

            findViewById<TextView>(R.id.tv_move_right_word).setOnClickListener {
                EditingText.cursor.moveRightWord()
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(findViewById<TextView>(R.id.tv_move_right_word))


        }
    }


    // 해당 레이아웃의 자식 View들을 array 형태로 가져온다
    private inline fun <reified T>getChildArray(targetView: ViewGroup, type:T): java.util.ArrayList<T> {
        val array = java.util.ArrayList<T>()

        val countChild = targetView.childCount
        for(i in 0 until countChild){
            if(targetView.getChildAt(i) is T){
                array.add(targetView.getChildAt(i) as T)
            }
        }
        return array
    }


    @SuppressLint("SetTextI18n", "CutPasteId")
    private fun initKeyboard(){
        val lines = getChildArray(rootLayout.findViewById<LinearLayout>(R.id.bottom_layout_english), LinearLayout(context))

        // 옵션키 - 일반 / Email
        llOptionKeys = lines[4]
        llOptionKeysEmail = lines[5]

        // number line (숫자열)
        val countNumberLineChild = lines[0].childCount
        for(i in 0 until countNumberLineChild){
            if(lines[0].getChildAt(i) is LinearLayout){
                val ll = lines[0].getChildAt(i) as LinearLayout
                val childTextViews = getChildArray(ll, TextView(context))
                childTextViews[1].text = KeypadData.numberLine[i]
                // 기기의 언어가 한국어 일때 2와 5는 앞에 숫자라는 글자를 붙여준다. (알파벳 e, o와 음성이 같아서)
                if(Locale.getDefault() == Locale.KOREA){
                    childTextViews[0].visibility = View.VISIBLE
                    childTextViews[0].text = "숫자"
                    childTextViews[2].setOnClickListener {
                        inputText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(childTextViews[2])
                }else{
                    childTextViews[0].visibility = View.GONE
                    childTextViews[1].setOnClickListener {
                        inputText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(childTextViews[1])
                }
            }else{
                if(lines[0].getChildAt(i) is TextView){
                    val textView = lines[0].getChildAt(i) as TextView
                    textView.text = KeypadData.numberLine[i]
                    textView.setOnClickListener{
                        inputText(KeypadData.numberLine[i])
                    }
                    keypadDialog.mWattHandTracking?.setAiClickListener(textView)
                }
            }

        }



        //패스워드 모드일때는 숫자헬퍼에 배정되는 숫자를 랜덤으로 섞는다.
        if(EditingText.currentKeypadType == TypeKeypad.Password){
            shuffleNumbers()
        }
        var countHelperNumber = 0


        //영문자 설정
        for(i in 1..3){
            val lineElements = getChildArray(lines[i], ConstraintLayout(context))

            //소문자
            val letterSmallTextViews = ArrayList<TextView>()
            //대문자
            val letterBigTextViews = ArrayList<TextView>()

            for(j in lineElements.indices){
                val englishHelper = lineElements[j].getChildAt(0) as TextView
                if(EditingText.currentKeypadType == TypeKeypad.Password){
                    // 패스워드 모드일때 랜덤으로 섞은 숫자를 숫자헬퍼에 배정한다.
                    englishHelper.text = shuffleHelperNumbers[countHelperNumber]
                }else{
                    // 정상적인 숫자배열을 숫자헬퍼로 배정한다.
                    englishHelper.text = originHelperNumbers[countHelperNumber]
                }

                countHelperNumber++

                // 음성입력을 위한 텍스트뷰 (클릭리스너는 여기에만 붙임) -> 대문자-소문자 번갈아가며 텍스트뷰를 음성인식하게 할때 인식이 잘 안되는 버그때문
                val letterText = lineElements[j].getChildAt(3) as TextView
                letterText.text = letterSmallLines[i - 1][j]

                // 소문자 텍스트 뷰 (키보드에 보여지기만 하는 텍스트뷰)
                val letterSmallText = lineElements[j].getChildAt(1) as TextView
                letterSmallText.text = letterSmallLines[i - 1][j]

                // 대문자 텍스트 뷰 (키보드에 보여지기만 하는 텍스트뷰)
                val letterBigText = lineElements[j].getChildAt(2) as TextView
                letterBigText.text = letterBigLines[i - 1][j]

                letterText.setOnClickListener {
                    var letter = letterText.text.toString()
                    if(isUpper)
                        letter = letter.toUpperCase(Locale.ROOT)
                    inputText(letter)
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(letterText)

                // 글자옆의 숫자헬퍼를 부를때의 처리
                englishHelper.setOnClickListener {
                    var letter = letterText.text.toString()
                    if(isUpper)
                        letter = letter.toUpperCase(Locale.ROOT)
                    inputText(letter)
                }
                keypadDialog.mWattHandTracking?.setAiClickListener(englishHelper)

                letterSmallTextViews.add(letterSmallText)
                letterBigTextViews.add(letterBigText)
            }

            letterSmallTextViewsList.add(letterSmallTextViews)
            letterBigTextViewsList.add(letterBigTextViews)
        }


        // 3번째 줄 옵션키 설정
        val lineElementsOptionLine3 = getChildArray(lines[3], TextView(context))
        lineElementsOptionLine3[0].text = context.resources.getString(R.string.custom_keypad_upper_txt)
        lineElementsOptionLine3[0].setOnClickListener {
            isUpper = !isUpper
            if(isUpper){
                lineElementsOptionLine3[0].text = context.resources.getString(R.string.custom_keypad_lower_txt)
            }else{
                lineElementsOptionLine3[0].text = context.resources.getString(R.string.custom_keypad_upper_txt)
            }
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOptionLine3[0])

        if(Locale.getDefault() == Locale.KOREA){
            lineElementsOptionLine3[1].visibility = View.VISIBLE
            lineElementsOptionLine3[1].text = context.resources.getString(R.string.custom_keypad_korea_txt)
            lineElementsOptionLine3[1].setOnClickListener {
                EditingText.currentKeypadType = TypeKeypad.Korean
            }
            keypadDialog.mWattHandTracking?.setAiClickListener(lineElementsOptionLine3[1])

        }else{
            lineElementsOptionLine3[1].visibility = View.GONE
        }

        // 4번째 줄 옵션키 설정
        val lineElementsOption = getChildArray(lines[4], TextView(context))
        for(j in lineElementsOption.indices){
            if(Locale.getDefault() == Locale.KOREA){
                lineElementsOption[j].text = KeypadData.optionLine[j]
            }else{
                lineElementsOption[j].text = KeypadData.optionLineEn[j]
            }
            setOptionKeyClickListener(j, lineElementsOption[j])
        }


        //Email 옵션키 설정 (5번째줄로 세팅하고 Email 모드일때 4번째줄을 숨김처리하고 5번째 줄을 보여지게 함)
        val optionKeysOnEmailKeys = getChildArray(lines[5], TextView(context))
        if(Locale.getDefault() == Locale.KOREA){
            //special key
            optionKeysOnEmailKeys[0].text = KeypadData.optionLine[0]
            //cancel key
            optionKeysOnEmailKeys[1].text = KeypadData.optionLine[4]
            //ok key
            optionKeysOnEmailKeys[2].text = KeypadData.optionLine[5]
        }else{
            //special key
            optionKeysOnEmailKeys[0].text = KeypadData.optionLineEn[0]
            //cancel key
            optionKeysOnEmailKeys[1].text = KeypadData.optionLineEn[4]
            //ok key
            optionKeysOnEmailKeys[2].text = KeypadData.optionLineEn[5]
        }
        setOptionKeyClickListener(0, optionKeysOnEmailKeys[0])
        setOptionKeyClickListener(4, optionKeysOnEmailKeys[1])
        setOptionKeyClickListener(5, optionKeysOnEmailKeys[2])


        val lineElementsOptionEmail = getChildArray(lines[5], LinearLayout(context))
        val firstTextViews = ArrayList<TextView>()
        val secondTextViews = ArrayList<TextView>()

        for(j in lineElementsOptionEmail.indices){
            firstTextViews.add(lineElementsOptionEmail[j].getChildAt(0) as TextView)
            secondTextViews.add(lineElementsOptionEmail[j].getChildAt(1) as TextView)
        }

        for(i in firstTextViews.indices){
            firstTextViews[i].text = KeypadData.optionLineEmailEn[i]
            secondTextViews[i].text = KeypadData.optionLineEmail[i]
            setOptionKeyEmailClickListener(i, firstTextViews[i])
            setOptionKeyEmailClickListener(i, secondTextViews[i])
            if(Locale.getDefault() == Locale.KOREA){
                secondTextViews[i].visibility = View.VISIBLE
            }else{
                // 언어가 영어일때는 한글 숨김
                secondTextViews[i].visibility = View.GONE
            }
        }


        //추가 명령어 설정
        if(Locale.getDefault() == Locale.KOREA){
            addCommand("한글전환"){
                EditingText.currentKeypadType = TypeKeypad.Korean
            }
            addCommand("영"){
                inputText("0")
            }
        }

        addCommand("dot"){
            inputText(".")
        }

    }


    private fun setOptionKeyEmailClickListener(index:Int, textView:TextView){
        textView.setOnClickListener {
            when(index){
                0 -> { // .com 닷컴
                    inputText(".com")
                }
                1 -> { // .co.kr 씨오점
                    inputText(".co.kr")
                }
                2 -> { // @ 앳
                    inputText("@")
                }
                3 -> { // . 점
                    inputText(".")
                }
            }
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
    }


    private fun setOptionKeyClickListener(index: Int, textView: TextView){
        textView.setOnClickListener {
            when(index){
                0 -> { // special key
                    EditingText.currentKeypadType = TypeKeypad.Special
                }
                1 -> { // .com key
                    inputText(".com")
                }
                2 -> { // period key
                    inputText(".")
                }
                3 -> { // space key
                    inputText(" ")
                }
                4 -> { // cancel key
                    EditingText.restoreText()
                    EditingText.dismissDialog()
                }
                5 -> { // ok key
                    EditingText.dismissDialog()
                }
            }
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)
    }


    // 음성명령어 추가
    private fun addCommand(commandText: String, onClickCallback: () -> Unit){
        val textView = TextView(context)
        textView.text = commandText
        textView.contentDescription = "hf_no_number"
        val lp = LinearLayout.LayoutParams(1, 1)
        textView.layoutParams = lp
        textView.setOnClickListener {
            onClickCallback()
        }
        keypadDialog.mWattHandTracking?.setAiClickListener(textView)

        rootLayout.findViewById<LinearLayout>(R.id.ll_command).addView(textView)
    }



    // 글자 입력할때는 이 메서드를 활용 -> 입력창의 커서 위치를 이동해 줘야 하기 때문
    private fun inputText(str: String){
        addTexts(str)
        EditingText.cursor.moveRight(str.length)
    }

    // 커서가 글자 끝에 있지 않고 중간에 있을 경우에 글자를 추가할 경우도 고려하여 만든 메서드
    private fun addTexts(str: String){
        val currentText = EditingText.editingText
        if(currentText.isEmpty()){
            EditingText.editingText = str
        } else{
            val nowPosition = EditingText.nowCursorPosition

            val begin = currentText.substring(0, nowPosition)
            val end = currentText.substring(nowPosition)

            var completeText = begin
            completeText += str
            completeText += end
            EditingText.editingText = completeText
        }
    }

    // 커서가 글자 끝에 있지 않고 중간에 있을 경우에 글자를 삭제할 경우도 고려하여 만든 메서드
    private fun subText(){
        val currentText = EditingText.editingText
        if(currentText.isEmpty())
            return

        var nowPosition = EditingText.nowCursorPosition

        if(nowPosition > currentText.length){
            nowPosition = currentText.length
        }

        if(nowPosition > 0){
            val completeText = currentText.removeRange(nowPosition - 1, nowPosition)
            EditingText.editingText = completeText
            EditingText.nowCursorPosition = nowPosition-1
        }
    }


    // 배열내의 숫자를 랜덤으로 섞어주는 메서드
    // 랜덤으로 숫자를 생성해서 26개를 배치하려면 한개 생성할때바다 중복되는지 체크해야하기 때문에
    // 있는 숫자에서 배열의 위치를 랜덤으로 교체하여 섞어주면 같은 결과를 가진다.
    private fun shuffleNumbers(){
        val r = Random()
        for(i in 0..25){
            val temp1 = shuffleHelperNumbers[i]
            val shuffleTargetNum = r.nextInt(26)
            if(i != shuffleTargetNum){
                val temp2 = shuffleHelperNumbers[shuffleTargetNum]
                shuffleHelperNumbers[i] = temp2
                shuffleHelperNumbers[shuffleTargetNum] = temp1
            }else{
                continue
            }
        }

    }


}