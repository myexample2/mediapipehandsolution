package com.peng.plant.wattviewer;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class ImageControll extends RelativeLayout {

    boolean error = false;
    public ZoomControll zoomViewer =new ZoomControll(getContext());;
    public ImageControll(Context context) {
        super(context);
    }

    public ImageControll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void showImage(ImageControll layout){

        if(error){
            zoomViewer.errorMessage();
            /*이미지 설정에 문제가있을경우 오류 메시지 화면에 출력 */
        }
        else{
            zoomViewer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
            zoomViewer.startViewer(zoomViewer,layout);

        }
    }
    public void count(int pageCount,int fullPage){
        zoomViewer.count(pageCount, fullPage);
    }
    public void pdfSetting(Context context,String filePath){
        error = false;
        zoomViewer.pdfSetting(context,filePath);
    }
    public void setImage(Bitmap bitmap){

        if(bitmap==null){
            error = true;
        }
        else{
            zoomViewer.setImage(bitmap);
        }
    }
    public void setImage(Uri uri){
        if(uri==null){
            error = true;
        }
        else {
            zoomViewer.setImage(uri);
        }
    }
    public void setImage(String string){
        if(string==null){
            error = true;
        }
        else{
            zoomViewer.setImage(string);
        }
    }
    public void setImage(ArrayList<Uri> uri){
        if(uri==null){
            error = true;
        }
        else{
            zoomViewer.setImage(uri);
        }
    }
    public void setStringData(ArrayList<String> string){
        if(string==null){
            error = true;
        }
        else{
            zoomViewer.setStringData(string);
        }
    }
    public void setBitmapData(ArrayList<Bitmap> bitmap) {
        if (bitmap == null) {
            error = true;
        } else {
            zoomViewer.setBitmapData(bitmap);
        }

    }

    public void setMinimap(boolean minimap) {
        zoomViewer.setMiniMapEnabled(minimap);

    }

}
