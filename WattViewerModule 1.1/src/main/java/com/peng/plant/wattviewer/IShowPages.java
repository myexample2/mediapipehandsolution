package com.peng.plant.wattviewer;

import android.graphics.Bitmap;

public interface IShowPages {

     Bitmap showPage(int index);
}
