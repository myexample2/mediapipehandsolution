**NAME**  
AR HANDS

**DESCRIPTION**  
카메라 프리뷰에 위치한 손가락을 감지하여 해당 포인트로 UI를 작업하는 라이브러리.

AAR 형식으로 배포하여 사용.

**SET UP**
project 폴더 구조 -> lib -> aar 파일 삽입
- ar_hands_v[version]/.aar
- hands.aar
- solution_core.aar

Xml 구조
- Root FrameLayout 안에 ar_hands_layout.xml을 include 한다.
    ex)
        <FrameLayout>
            <include
            layout="@layout/ar_hands_layout"/>
        </FrameLayout>

- Gradle 
    // MediaPipe deps
    implementation 'com.google.flogger:flogger:0.3.1'
    implementation 'com.google.flogger:flogger-system-backend:0.3.1'
    implementation 'com.google.code.findbugs:jsr305:3.0.2'
    implementation 'com.google.guava:guava:27.0.1-android'
    implementation 'com.google.protobuf:protobuf-java:3.11.4'
    // CameraX core library
    def camerax_version = "1.0.0-beta10"
    implementation "androidx.camera:camera-core:$camerax_version"
    implementation "androidx.camera:camera-camera2:$camerax_version"
    implementation "androidx.camera:camera-lifecycle:$camerax_version"

**API**

Begin
- startHand(Activity, View, GestureListener, ClickInterface)

Stop
- stopHands(void)

Click
- setAiClickListener(View) 함수 사용

Scroll
- setAiScrollUIListener(View) 함수 사용
- 등록 한 View에 대한 스크롤 값 -> HandGestureListener 인터페이스 move()로 값 전달

Zoom
- 등록 한 View에 대한 스크롤 값 -> HandGestureListener 인터페이스 zoom()로 값 전달

Gesture Icon 색상 변경
- setGestureIconColorReverse(bool) 


